

class Player {
    constructor(name, color, score, lifetimeScore, lifetimeWins, element){
        this.name = name;
        this.color = color;
        this.active = true;
        this.score = score;
        this.lifetimeScore = lifetimeScore;
        this.lifetimeWins = lifetimeWins;
        
        this.element = element;
        this.markerElement = element.querySelector(".marker");
        this.nameElement = element.querySelector(".player-name");
        this.scoreElement = element.querySelector(".score");
        this.lifetimeScoreElement = element.querySelector(".lifetime-score")
        this.lifetimeWinsElement = element.querySelector(".lifetime-wins");
    }

    markInactive(){
        this.markerElement.classList.add('inactive');
        this.markerElement.innerHTML = '<i class="fa-solid fa-xmark"></i>';
        this.active = false;
    }

    markActive(){
        this.markerElement.classList.remove('inactive');
        this.markerElement.innerHTML = '';
        this.active = true;
    }

    markReady(){
        this.markActive;
        this.markerElement.classList.add('ready');
        this.markerElement.innerHTML = '<i class="fa-solid fa-check"></i>';
    }

    markGone(){
        this.active = false;
        this.nameElement.classList.add('gone');
        this.markerElement.innerHTML = '';
    }

    markRejoined(){
        this.markActive();
        this.nameElement.classList.remove('gone');
    }

}

class PlayerManager {
    constructor() {
        this.players = {};
        this.playerList = [];
        this.myHand = [];
        this.playerCount = 0;
        this.activePlayerCount = 0;
    }

    addPlayer(name, color, points, lifetimePoints, lifetimeWins, element){
        this.players[name] = new Player(name, color, points, lifetimePoints, lifetimeWins, element);
        this.playerList.push(name);
        this.playerCount += 1;
        this.activePlayerCount += 1;
    }

    playerInactive(name){
        if(this.playerList.length == 0){return}
        let player = this.players[name];
        if(player.active){
            this.activePlayerCount -= 1;
        }
        player.markInactive();
    }

    playerActive(name){
        if(this.playerList.length == 0){return}
        let player = this.players[name];
        if(!player.active){
            this.activePlayerCount += 1;
        }
        player.markActive();
    }

    playerReady(name){
        if(this.playerList.length == 0){return}
        let player = this.players[name];
        if(!player.active){
            this.activePlayerCount += 1;
        }
        player.markReady();
    }

    playerLeft(name){
        if(this.playerList.length == 0){return}
        let player = this.players[name];
        if(player.active){
            this.activePlayerCount -= 1;
        }
        player.markGone();
    }

    playerRejoined(name){
        if(this.playerList.length == 0){return}
        this.activePlayerCount += 1;
        this.players[name].markRejoined();
    }

    getRandomPlayer(){
        return players[this.playerList[Math.floor(Math.random() * this.playerList.length)]];
    }

    getPlayerWithMostPoints(){
        highest = Object.keys(this.players)[0]
        for(let player in players){
            if(player.score > highest.score){
                highest = player;
            }
        }
        return highest;
    }

    getPlayerWithLeastPoints(){
        lowest = Object.keys(this.players)[this.players.length-1]
        for(let player in players){
            if(player.score < lowest.score){
                lowest = player;
            }
        }
        return lowest;
    }

    incrementScore(name){
        if(this.playerList.length == 0){return}
        let player = this.players[name];
        player.score += 1;
        player.lifetimeScore += 1;
        player.scoreElement.textContent = player.score;
        player.lifetimeScoreElement.textContent = player.lifetimeScore;
    }

    incrementWins(name){
        if(this.playerList.length == 0){return}
        let player = this.players[name];
        player.lifetimeWins += 1;
        player.lifetimeWinsElement.textContent = player.lifetimeWins;
    }
    
}