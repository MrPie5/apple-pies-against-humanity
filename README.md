**Apple Pies Against Humanity** is a browser-based Cards Against Humanity / Apples to Apples clone made by Mr Pie 5. It is heavily inspired by a Starcraft II arcade game called Apples Against Humanity by azala, which is the best variation of the game my friends and I have played. The problem with that is it comes with the baggage of having Starcraft II installed, which is free but quite large and quite system-intensive for a text game, so it's often difficult to invite new people to join us. Something browser-based would be portable and accessible, but I have been unable to find any browser-based game that does what that variation got right, not even the one made by its creator, so I did it myself.

This app is made with Node.js, Express, MongoDB, Socket.IO, a bit of jQuery, and some other libraries.

## What does this version do that others don't?
Fair question, there's tons of clones, so why'd I go through the trouble?

The biggest point would be custom cards. Ease of creating and using custom cards, both white and black, are what made the SC2 version so much fun and greatly enhances replayability; just type a simple chat command and you've got a new card in your deck, to be shuffled in the next time you start a game. Got a sudden card idea while you're playing? Add it, just like that.

Smaller points of note are the Ace Attorney music and sound effects, the interface, and just the way the game flows overall.

## Using custom cards
Each player has a personal deck of their own custom cards, both white and black, that persist between games. At the beginning of each game the custom decks of all players are shuffled together into a communal deck along with the default cards, so you can draw cards someone else wrote and vice versa.

To add a custom card to your deck, open the chat during a game and type `-a [text]` to add an answer, such as `-a Nicolas Cage.`. For questions, type `-q [number of blanks] [text]`, such as `-q 2 I can't believe _____ was _____ all along!` (note that adding blanks to the question is entirely aesthetic; the number of answers required here is determined by the 2 at the beginning).

If you make a mistake or just have a change of heart and decide your card sucks, you can type `-undo` to remove the last custom card you added. Note that this only goes one level deep, so once you add another custom, you can no longer undo the card you added before that.

### Custom card conventions
There are a couple standard conventions that cards observe, and while it's not required that you follow them, they'll make your cards look better.
* Sentence case: capitalize the first letter and nothing else unless there's reason to capitalize further, such as proper nouns or extra capitals being part of the joke/reference.
* End answers with a period if they don't already end with another punctuation mark. e.g. `Nicolas Cage.` instead of `Nicolas Cage`

## Additional features
- Global custom deck shuffling: normally only the custom decks of players in the current game will be used, but if this option is enabled then the custom decks of all registered users will be used. Note that this increases the chances that you will encounter inside jokes and references you don't understand, but you can always just discard those.
- Rando Cardrissian: adds an extra "player" to the game who will always play random answers to questions (but will not vote), and if he wins a round it means you and your friends aren't funny and should feel bad.

### Planned future features
- Duplicate removal: remove duplicate cards when shuffling the deck (#21)
- Deck editor: add and remove cards from your custom deck outside of the game (#12)

## Credits
- azala for their original Starcraft II version
- Yong Que and their capstone partners (sorry, can't find their names) for their work on RGO (private repo), which served as a great starting point for the user account system, and provided an example framework for socket communication.
- Everyone who has and is helping me test, including harpiesd96, confusedTurtle, Ninfendo1, and Herpaderp
- Base black and white cards were taken from https://github.com/ajanata/PretendYoureXyzzy/blob/master/cah_cards.sql
