// Taken from https://github.com/ajanata/PretendYoureXyzzy/blob/master/cah_cards.sql by Andy Janata
// Licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// For more information, see http://creativecommons.org/licenses/by-nc-sa/3.0/

module.exports = [
	{"text": `Critics are raving about HBO's new Game of Thrones spin-off, "____ of ____."`, "answers": 2, "owner": ""},
	{"text": `Your father was a powerful wizard, Harry. Before he died, he left you something very precious: ____.`, "answers": 1, "owner": ""},
	{"text": `Having tired of poetry and music, the immortal elves now fill their days with ____.`, "answers": 1, "owner": ""},
	{"text": `And in the end, the dragon was not evil; he just wanted ____.`, "answers": 1, "owner": ""},
	{"text": `Who blasphemes and bubbles at the center of all infinity, whose name no lips dare speak aloud, and who gnaws hungrily in inconceivable, unlighted chambers beyond time?`, "answers": 1, "owner": ""},
	{"text": `Legend tells of a princess who has been asleep for a thousand years and can only be awoken by ____.`, "answers": 1, "owner": ""},
];