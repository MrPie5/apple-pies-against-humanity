// Taken from https://github.com/ajanata/PretendYoureXyzzy/blob/master/cah_cards.sql by Andy Janata
// Licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// For more information, see http://creativecommons.org/licenses/by-nc-sa/3.0/

module.exports = [
	{"text": `It's not delivery. It's ____.`, "answers": 1, "owner": ""},
	{"text": `Don't miss Rachel Ray's hit new show, Cooking with ____.`, "answers": 1, "owner": ""},
	{"text": `I'm Bobby Flay, and if you can't stand ____, get out of the kitchen!`, "answers": 1, "owner": ""},
	{"text": `Now on Netflix: Jiro Dreams of ____.`, "answers": 1, "owner": ""},
	{"text": `Aw babe, your burps smell like ____!`, "answers": 1, "owner": ""},
	{"text": `Excuse me, waiter. Could you take this back? This soup tastes like ____.`, "answers": 1, "owner": ""},
];