// Taken from https://github.com/ajanata/PretendYoureXyzzy/blob/master/cah_cards.sql by Andy Janata
// Licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// For more information, see http://creativecommons.org/licenses/by-nc-sa/3.0/

module.exports = [
	{"text": `When you go to the polls on Tuesday, remember: a vote for me is a vote for ____.`, "answers": 1, "owner": ""},
	{"text": `Senator, I trust you enjoyed ____ last night. Now, can I count on your vote?`, "answers": 1, "owner": ""},
	{"text": `Trump's great! Trump's got ____. I love that.`, "answers": 1, "owner": ""},
	{"text": `It's 3 AM. The red phone rings. It's ____. Who do you want answering?`, "answers": 1, "owner": ""},
	{"text": `According to Arizona's stand-your-ground law, you're allowed to shoot someone if they're ____.`, "answers": 1, "owner": ""},
];