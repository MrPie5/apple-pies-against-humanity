// Taken from https://github.com/ajanata/PretendYoureXyzzy/blob/master/cah_cards.sql by Andy Janata
// Licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// For more information, see http://creativecommons.org/licenses/by-nc-sa/3.0/

module.exports = [
	{"text": `After blacking out during New Year's Eve, I was awoken by ____.`, "answers": 1, "owner": ""},
	{"text": `What keeps me warm during the cold, cold winter?`, "answers": 1, "owner": ""},
	{"text": `Wake up, America. Christmas is under attack by secular liberals and their ____.`, "answers": 1, "owner": ""},
	{"text": `Every Christmas, my uncle gets drunk and tells the story about ____.`, "answers": 1, "owner": ""},
	{"text": `Jesus is ____.`, "answers": 1, "owner": ""},
	{"text": `This holiday season, Tim Allen must overcome his fear of ____ to save Christmas.`, "answers": 1, "owner": ""},
	{"text": `On the third day of Christmas, my true love gave to me: three French hens, two turtle doves, and ____.`, "answers": 1, "owner": ""},
	{"text": `Because they are forbidden from masturbating, Mormons channel their repressed sexual energy into ____.`, "answers": 1, "owner": ""},
	{"text": `Revealed: Why He Really Resigned! Pope Benedict's Secret Struggle with ____!`, "answers": 1, "owner": ""},
	{"text": `Blessed are you, Lord our God, creator of the universe, who has granted us ____.`, "answers": 1, "owner": ""},
	{"text": `Kids these days with their iPods and their internet. In my day, we all needed to pass the time was ____.`, "answers": 1, "owner": ""},
	{"text": `GREETINGS HUMANS! I AM ____ BOT!  EXECUTING PROGRAM!`, "answers": 1, "owner": ""},
	{"text": `But wait, there's more! If you order ____ in the next 15 minutes, we'll throw in ____ absolutely free!`, "answers": 2, "owner": ""},
	{"text": `Here's what you can expect for the new year. Out: ____. In: ____.`, "answers": 2, "owner": ""},
	{"text": `What's the one thing that makes an elf instantly ejaculate?`, "answers": 1, "owner": ""},
	{"text": `I really hope my grandma doesn't ask me to explain ____ again.`, "answers": 1, "owner": ""},
	{"text": `Behold the Four Horsemen of the Apocalypse! War, Famine, Death, and ____.`, "answers": 1, "owner": ""},
	{"text": `Honey, Mommy and Daddy love you very much.But apparently Mommy loves ____ more than she loves Daddy.`, "answers": 1, "owner": ""},
	{"text": `A curse upon thee! Many years from now, just when you think you're safe, ____ shall turn into ____.`, "answers": 2, "owner": ""},
	{"text": `Dear Mom and Dad, Camp is fun. I like capture the flag. Yesterday, one of the older kids taught me about ____. I love you, Casey`, "answers": 1, "owner": ""},
	{"text": `Today on Buzzfeed: 10 Pictures of ____ That Look Like ____!`, "answers": 2, "owner": ""},
	{"text": `Why am I so tired?`, "answers": 1, "owner": ""},
	{"text": `Donna, pick up my dry cleaning and get my wife something for christmas. I think she likes ____.`, "answers": 1, "owner": ""},
	{"text": `It's beginning to look a lot like ____.`, "answers": 1, "owner": ""},
];