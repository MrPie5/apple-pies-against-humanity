// Taken from https://github.com/ajanata/PretendYoureXyzzy/blob/master/cah_cards.sql by Andy Janata
// Licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// For more information, see http://creativecommons.org/licenses/by-nc-sa/3.0/

module.exports = [
	{"text": `Kevin Bacon Bits.`, "owner": ""},
	{"text": `Being emotionally and physically dominated by Gordon Ramsay.`, "owner": ""},
	{"text": `A belly full of hard-boiled eggs.`, "owner": ""},
	{"text": `Kale farts.`, "owner": ""},
	{"text": `Clamping down on a gazelle's jugular and tasting its warm life waters.`, "owner": ""},
	{"text": `A table for one at The Cheesecake Factory.`, "owner": ""},
	{"text": `The hot dog I put in my vagina ten days ago.`, "owner": ""},
	{"text": `The Dial-A-Slice Apple Divider from Williams-Sonoma.`, "owner": ""},
	{"text": `Oreos for dinner.`, "owner": ""},
	{"text": `A joyless vegan patty.`, "owner": ""},
	{"text": `Soup that's better than pussy.`, "owner": ""},
	{"text": `The Hellman's Mayonnaise Corporation.`, "owner": ""},
	{"text": `Going vegetarian and feeling so great all the time.`, "owner": ""},
	{"text": `Not knowing what to believe anymore about butter.`, "owner": ""},
	{"text": `A sobering quantity of chili cheese fries.`, "owner": ""},
	{"text": `Licking the cake batter off of grandma's fingers.`, "owner": ""},
	{"text": `Real cheese flavor.`, "owner": ""},
	{"text": `Swishing the wine around and sniffing it like a big fancy man.`, "owner": ""},
	{"text": `Sucking down thousands of pounds of krill every day.`, "owner": ""},
	{"text": `The inaudible screams of carrots.`, "owner": ""},
	{"text": `Committing suicide at the Old Country Buffet.`, "owner": ""},
	{"text": `What to do with all of this chocolate on my penis.`, "owner": ""},
	{"text": `Father's forbidden chocolates.`, "owner": ""},
	{"text": `Jizz Twinkies.`, "owner": ""},
];