// Taken from https://github.com/ajanata/PretendYoureXyzzy/blob/master/cah_cards.sql by Andy Janata
// Licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// For more information, see http://creativecommons.org/licenses/by-nc-sa/3.0/

module.exports = [
	{"text": `Dinosaurs who wear armor and you ride them and they kick ass.`, "owner": ""},
	{"text": `Accidentally conjuring a legless horse that can't stop ejaculating.`, "owner": ""},
	{"text": `Shitting in a wizard's spell book and jizzing in his hat.`, "owner": ""},
	{"text": `A Hitachi Magic Wand.`, "owner": ""},
	{"text": `Reading <i>The Hobbit</i> under the covers while mom and dad scream at each other downstairs.`, "owner": ""},
	{"text": `How hot Orlando Bloom was in <i>Lord of the Rings.</i>`, "owner": ""},
	{"text": `A mysterious, floating orb.`, "owner": ""},
	{"text": `Shooting a wizard with a gun.`, "owner": ""},
	{"text": `Hodor.`, "owner": ""},
	{"text": `Make-believe stories for autistic white men.`, "owner": ""},
	{"text": `A magical kingdom with dragons and elves and no black people.`, "owner": ""},
	{"text": `The card Neil Gaiman wrote: "Three elves at a time."`, "owner": ""},
	{"text": `Gender equality.`, "owner": ""},
	{"text": `Going on an epic adventure and learning a valuable lesson about friendship.`, "owner": ""},
	{"text": `True love's kiss.`, "owner": ""},
	{"text": `Eternal darkness.`, "owner": ""},
	{"text": `The all-seeing Eye of Sauron.`, "owner": ""},
	{"text": `Bathing naked in a moonlit grove.`, "owner": ""},
	{"text": `Handcuffing a wizard to a radiator and dousing him with kerosene.`, "owner": ""},
	{"text": `Kneeing a wizard in the balls.`, "owner": ""},
	{"text": `A ghoul.`, "owner": ""},
	{"text": `A weed elemental who gets everyone high.`, "owner": ""},
	{"text": `A gay sorcerer who turns everyone gay.`, "owner": ""},
	{"text": `A CGI dragon.`, "owner": ""},
	{"text": `Freaky, pan-dimensional sex with a demigod.`, "owner": ""},
	{"text": `A dwarf who won't leave you alone until you compare penis sizes.`, "owner": ""},
];