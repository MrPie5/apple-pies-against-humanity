// Taken from https://github.com/ajanata/PretendYoureXyzzy/blob/master/cah_cards.sql by Andy Janata
// Licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
// For more information, see http://creativecommons.org/licenses/by-nc-sa/3.0/

module.exports = [
	{"text": `Donald Trump holding his nose while he eats pussy.`, "owner": ""},
	{"text": `Black lives mattering.`, "owner": ""},
	{"text": `Kicking the middle class in the balls with a regressive tax code.`, "owner": ""},
	{"text": `Slapping Ted Cruz over and over.`, "owner": ""},
	{"text": `Eating the president's pussy.`, "owner": ""},
	{"text": `Keeping the government out of my vagina.`, "owner": ""},
	{"text": `The fact that Hillary Clinton is a woman.`, "owner": ""},
	{"text": `Increasing economic inequality and political polarization.`, "owner": ""},
	{"text": `The Bernie Sanders revolution.`, "owner": ""},
	{"text": `A beautiful, ever-expanding circle of inclusivity that will never include Republicans.`, "owner": ""},
	{"text": `Letting Bernie Sanders rest his world-weary head on your lap.`, "owner": ""},
	{"text": `The systemic disenfranchisement of black voters.`, "owner": ""},
	{"text": `Actually voting for Donald Trump to be President of the actual United States.`, "owner": ""},
	{"text": `Growing up and becoming a Republican.`, "owner": ""},
	{"text": `A liberal bias.`, "owner": ""},
	{"text": `Full-on socialism.`, "owner": ""},
	{"text": `Hating Hillary Clinton.`, "owner": ""},
	{"text": `Jeb!`, "owner": ""},
	{"text": `Conservative talking points.`, "owner": ""},
	{"text": `Courageously going ahead with that racist comment.`, "owner": ""},
	{"text": `The good, hardworking people of Dubuque, Iowa.`, "owner": ""},
	{"text": `Dispelling with this fiction that Barack Obama doesn't know what he's doing.`, "owner": ""},
	{"text": `Shouting the loudest.`, "owner": ""},
	{"text": `Sound fiscal policy.`, "owner": ""},
];