class Player {
    constructor(name, color, socket, db, isGuest) {
        // db is for a mongodb document
        this.name = name;
        this.color = color;
        this.socket = socket;
        this.hand = [];
        this.discards = [];
        this.selection = [];
        this.points = 0;
        this.active = true;
        this.inGame = true;
        this.isGuest = isGuest;
        this.lifetimePoints = (db) ? db.rounds_won : 0;
        this.lifetimeWins = (db) ? db.games_won : 0;
        this.customAnswerCount = (db) ? db.answer_deck.length : 0;
        this.customQuestionCount = (db) ? db.question_deck.length : 0;
        this.bannedAuthors = (db) ? db.banned_authors : [];
        this.lastCustom = undefined;
    }
}

module.exports = Player;