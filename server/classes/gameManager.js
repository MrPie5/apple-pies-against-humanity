// If a player is disconnected during the transition phase, we get a race condition. The player count has gone down, and the counter in the custom adding is comparing to player count.
// I don't yet see a way to do this without the counter approach, because the foreach is asynchronous, and I haven't gotten await to work with it. So if we don't use the counter, the deck is shuffled before
// player customs are added in.



const mongoGame = require("../mongodb_models/game_model.js");
const mongoUser = require("../mongodb_models/user_model.js");

const eventEmitter = require("../events.js");

const answersBase = require('../cards/answers/a-base.js');
const answersGreen = require('../cards/answers/a-green.js');
const answersBlue = require('../cards/answers/a-blue.js');
const answersRed = require('../cards/answers/a-red.js');
const answers2016 = require('../cards/answers/a-2016.js');
const answersFantasy = require('../cards/answers/a-fantasy.js');
const answersFood = require('../cards/answers/a-food.js');
const answersHoliday = require('../cards/answers/a-holiday.js');

const questionsBase = require('../cards/questions/q-base.js');
const questionsGreen = require('../cards/questions/q-green.js');
const questionsBlue = require('../cards/questions/q-blue.js');
const questionsRed = require('../cards/questions/q-red.js');
const questions2016 = require('../cards/questions/q-2016.js');
const questionsFantasy = require('../cards/questions/q-fantasy.js');
const questionsFood = require('../cards/questions/q-food.js');
const questionsHoliday = require('../cards/questions/q-holiday.js');

const Player = require('./player.js');

const Analytics = require('./analytics.js');

const selectionLength = 45000;
const longSelectionLength = 60000;
const votingLength = 40000;
const longVotingLength = 60000;
const winLength = 5000;
const tieLength = 5000;
const gameOverLength = 10000;
const selfDestructLength = 10000;

const rando = "Rando Cardrissian";

const colors = ["red", "blue", "green", "purple", "orange", "yellow", "lightblue", "pink"];

class GameManager {
	constructor(id, name, io) {
		this.io = io;
		this.players = {};
		this.playerQueue = [];
		this.numActivePlayers = 0;
		this.name = name;
		this.roomID = id;
		this.cardsOnTable = [];
		this.whiteCards = [];
		this.blackCards = [];
		this.currentBlack = undefined;
		this.whiteDiscards = [];
		this.blackDiscards = [];
		this.phase = "lobby";
		this.votes = [];
		this.waiting = true;
		this.waitingFor = [];
		this.waitingForSkip = [];
		this.currentVotesToSkip = 0;
		this.votesNeededToSkip = 0;
		this.availableColors = [...colors];
		this.round = 0;
		this.timeLeft = 0;

		this.localCustomAnswerCount = 0;
		this.localCustomQuestionCount = 0;

		// we store our setTimeouts here so we can end them early if everyone is ready or the game needs to be deleted
		this.countdown = undefined;
		this.countdownInterval = undefined;
		this.skipTimer = undefined;
		this.selfDestructTimer = undefined;

		this.settings = {
			pointsToWin: GameManager.basePoints,
			handSize: GameManager.baseHandSize,
			globalCustoms: false,
			randoCardrissian: false,
			decksEnabled: {
				base: true,
				green: false,
				blue: false,
				red: false,
			}
		}

		// asyncronously re-count the customs and afterwards send the updated value to replace the old one
		io.to(id).emit("update global custom counts", GameManager.updateCustomCardCount());
	}

	static chatCharacterLimit = 150;
	static minResponses = 1;
	static maxResponses = 3;
	static maxHandSize = 15;
	static minHandSize = 2;
	static baseHandSize = 7;
	static maxPoints = 99;
	static minPoints = 1;
	static basePoints = 10;
	static pointsForWinToCount = 5;
	static maxPlayers = 8;

	static standardDecks = {
		base: {
			name: "Base",
			enabledByDefault: true,
			answerCount: answersBase.length,
			questionCount: questionsBase.length
		},
		green: {
			name: "Green",
			enabledByDefault: false,
			answerCount: answersGreen.length,
			questionCount: questionsGreen.length
		},
		blue: {
			name: "Blue",
			enabledByDefault: false,
			answerCount: answersBlue.length,
			questionCount: questionsBlue.length
		},
		red: {
			name: "Red",
			enabledByDefault: false,
			answerCount: answersRed.length,
			questionCount: questionsRed.length
		},
		election2016: {
			name: "2016 Election",
			enabledByDefault: false,
			answerCount: answers2016.length,
			questionCount: questions2016.length
		},
		fantasy: {
			name: "Fantasy",
			enabledByDefault: false,
			answerCount: answersFantasy.length,
			questionCount: questionsFantasy.length
		},
		food: {
			name: "Food",
			enabledByDefault: false,
			answerCount: answersFood.length,
			questionCount: questionsFood.length
		},
		holiday: {
			name: "Holiday",
			enabledByDefault: false,
			answerCount: answersHoliday.length,
			questionCount: questionsHoliday.length
		},
	}


	static updateCustomCardCount() {
		mongoUser.find({}, function(err, users){
			let counts = {answers:0, questions:0};
			if(!err){
				users.forEach(function(playerdb){
					counts.answers += playerdb.answer_deck.length;
					counts.questions += playerdb.question_deck.length;
				});
			}
			GameManager.customCardCount = counts;
			return counts;
		})
	}

	static customCardCount = {};


	async start() {
		let that = this;

		// for each player, add their custom cards to the deck
		let counter = 0;
		let answersCounter = [];
		let questionsCounter = [];

		this.whiteCards = [].concat(
			(this.settings.decksEnabled.base) ? answersBase : [],
			(this.settings.decksEnabled.green) ? answersGreen : [],
			(this.settings.decksEnabled.blue) ? answersBlue : [],
			(this.settings.decksEnabled.red) ? answersRed : [],
			(this.settings.decksEnabled.election2016) ? answers2016 : [],
			(this.settings.decksEnabled.fantasy) ? answersFantasy : [],
			(this.settings.decksEnabled.food) ? answersFodo : [],
			(this.settings.decksEnabled.holiday) ? answersHoliday : [],
		)

		this.blackCards = [].concat(
			(this.settings.decksEnabled.base) ? questionsBase : [],
			(this.settings.decksEnabled.green) ? questionsGreen : [],
			(this.settings.decksEnabled.blue) ? questionsBlue : [],
			(this.settings.decksEnabled.red) ? questionsRed : [],
			(this.settings.decksEnabled.election2016) ? questions2016 : [],
			(this.settings.decksEnabled.fantasy) ? questionsFantasy : [],
			(this.settings.decksEnabled.food) ? questionsFodo : [],
			(this.settings.decksEnabled.holiday) ? questionsHoliday : [],
		)

		if(this.settings.randoCardrissian){
			await mongoUser.findOne({name:rando}, function(err, playerdb){
				that.players[rando] = new Player(rando, "rainbow", {id: "Rando"}, playerdb);
			});
		}

		console.log("start adding player customs");
		if(this.settings.globalCustoms){
			mongoUser.find({}, function(err, users){
				users.forEach(function(playerdb){
					// if the player is in the current game, assign their color, otherwise give them "gone" (only for the purposes of coloring them in the chat message)
					let color;
					if(that.players[playerdb.name]){
						color = that.players[playerdb.name].color;
					}
					else {
						color = "gone";
					}

					// Only walk their deck and add them to the counter if they actually have a deck, or are in the current game
					if(playerdb.answer_deck.length > 0 || that.players[playerdb.name]){
						playerdb.answer_deck.forEach(function(card){
							that.whiteCards.push({"text": card, "owner": playerdb.name});
						});
						answersCounter.push({name:playerdb.name, count:playerdb.answer_deck.length, color: color});
					}

					if(playerdb.question_deck.length > 0 || that.players[playerdb.name]){
						playerdb.question_deck.forEach(function(card){
							card["owner"] = playerdb.name;
							that.blackCards.push(card);
						});
						questionsCounter.push({name:playerdb.name, count:playerdb.question_deck.length, color: color})
					}

					counter++;
					if(counter === users.length) {
						console.log("done adding player customs");

						// shuffle the decks
						that.shuffleAll();
						that.playerCheck(answersCounter, questionsCounter);
					}
				});
			});
		}
		else {
			this.playerQueue.forEach(function(playerName){
				mongoUser.findOne({name:playerName}, function(err, playerdb) {
					if(err){console.log(err);}
					if(playerdb){
						playerdb.answer_deck.forEach(function(card){
							that.whiteCards.push({"text": card, "owner": playerName});
						});
						answersCounter.push({name:playerdb.name, count:playerdb.answer_deck.length, color: that.players[playerName].color});

						playerdb.question_deck.forEach(function(card){
							card["owner"] = playerName;
							that.blackCards.push(card);
						});
						questionsCounter.push({name:playerdb.name, count:playerdb.question_deck.length, color: that.players[playerName].color})
					}

					counter++;
					if(counter === that.playerQueue.length) {
						console.log("done adding player customs");

						// shuffle the decks
						that.shuffleAll();
						that.playerCheck(answersCounter, questionsCounter);
					}
				});
			});
		}
	}

	processMagicWords(cardText){
		// replace {@r} with a random player in the current game
		if(cardText.includes("{@r}")){
			cardText = cardText.replace("{@r}", this.playerQueue[Math.floor(Math.random()*this.playerQueue.length)]);
		}
		// replace {@w} with the winning player
		if(cardText.includes("{@w}")){
			let name = this.playerQueue[0]; // default to first player
			let points = this.players[name].points;
			this.forEachPlayer(function(playerName){
				if(this.players[playerName].points > points){
					name = playerName;
					points = this.players[playerName].points;
				}
			}.bind(this));
			cardText = cardText.replace("{@w}", name);
		}
		// replace {@l} with the losing player
		if(cardText.includes("{@l}")){
			let name = this.playerQueue[this.playerQueue.length-1]; // default to last player
			let points = this.players[name].points;
			this.forEachPlayer(function(playerName){
				if(this.players[playerName].points < points){
					name = playerName;
					points = this.players[playerName].points;
				}
			}.bind(this));
			cardText = cardText.replace("{@l}", name);
		}
		return cardText;
	}

	playerCheck(answersCounter, questionsCounter){
		let that = this;
		// wait a few seconds for all players to reconnect after page move
		console.log("waiting");
		this.numActivePlayers = 0;
		this.countdown = setTimeout(function() {
			console.log("done waiting");
			this.setPhase("checking");

			if(this.round == 0){
				// send everyone the player list
				this.distributePlayerList();
			}

			that.forEachPlayer(function(playerName){
				let player = that.players[playerName];

				if(!player.socket){
					console.log(`${playerName} failed to reconnect on page change`);
					that.removePlayer(playerName);
				}
				else {
					if(that.round == 0){
						that.io.to(player.socket.id).emit('game info', {
							playerName: playerName,
							gameName: that.name,
							playerCount: Object.keys(that.players).length
						});
					}
				}
				that.numActivePlayers += 1;
			});
			if(this.settings.randoCardrissian){
				this.numActivePlayers -= 1;
			}

			this.calculateVotesToSkip();
			this.io.to(this.roomID).emit('skip counter', this.currentVotesToSkip, this.votesNeededToSkip);

			let chatMessage = "Custom answers: ";
			answersCounter.forEach(function(player){
				chatMessage += `<span class="player-name ${player.color}">${player.name}</span>&nbsp;${player.count}, `;
			});
			chatMessage = chatMessage.slice(0,-2) 
			chatMessage += `<br/>Shuffled ${this.whiteCards.length} answer cards.<br/>Custom questions: `;
			questionsCounter.forEach(function(player){
				chatMessage += `<span class="player-name ${player.color}">${player.name}</span>&nbsp;${player.count}, `;
			});
			chatMessage = chatMessage.slice(0,-2);
			chatMessage += `<br/>Shuffled ${this.blackCards.length} question cards.`;

			// send this as a sender-less message with HTML allowed
			this.io.to(that.roomID).emit('chat message', chatMessage, undefined, true);

			// make sure the chosen settings allow for enough cards, and if not, add the base decks
			if(this.whiteCards.length < (this.playerQueue.length * this.handSize * 2)) {
				this.whiteCards = this.whiteCards.concat(answersBase);
				this.io.to(that.roomID).emit('chat message', "The current game settings do not include enough answer cards. A copy of the base answer deck has been shuffled in to prevent issues.");
				this.shuffle(this.whiteCards);
			}
			if(this.blackCards.length < 2) {
				this.blackCards = this.blackCards.concat(questionsBase);
				this.io.to(that.roomID).emit('chat message', "The current game settings do not include enough question cards. A copy of the base question deck has been shuffled in to prevent issues.");
				this.shuffle(this.blackCards);
			}

			this.io.to(that.roomID).emit('round start');

			if(this.round != -1) {
				this.selectionPhase();
				this.countdownInterval = setInterval(function(){
					this.timeLeft -= 1000;
				}.bind(this), 1000);
			}
		}.bind(this), 2000);

	}

	// selection -> vote -> tie -> winner (if enough points, -> gameOver), repeat

	selectionPhase() {
		// We do an extra check for empty here because there's a small chance that it won't work when players leave
		if(this.checkForEmptyGame()){this.startSelfDestruct()};
		let that = this;
		this.setPhase("selection");
		console.log(`${this.name} entered ${this.phase} phase`);

		this.io.to(this.roomID).emit("update phase", this.phase);
		this.currentBlack = this.drawBlack();
		this.io.to(this.roomID).emit('distribute black', this.currentBlack);
		this.io.to(this.roomID).emit("countdown", ((this.currentBlack.answers == 1) ? selectionLength : longSelectionLength), Date.now());

		this.waitingForSkip = [];
		this.waitingFor = [];

		this.forEachPlayer(function(playerName){
			that.distributeWhite(playerName);
			if(that.players[playerName].active && playerName != rando){
				that.waitingFor.push(playerName);
				that.waitingForSkip.push(playerName);
			}
		});

		if(this.settings.randoCardrissian){
			let randoPlayer = this.players[rando];
			for(let i = 0; i < that.currentBlack.answers; i++){
				randoPlayer.hand.push(this.drawWhite());
				randoPlayer.selection.push(i);
			}
			this.receiveSelection(rando, randoPlayer.selection);
		}

		this.waiting = true;

		// wait for the duration of roundLength, then calculate the end of the phase and move to the next
		this.countdown = setTimeout(this.afterSelection.bind(this), ((this.currentBlack.answers == 1) ? selectionLength : longSelectionLength));
		this.timeLeft = ((this.currentBlack.answers == 1) ? selectionLength : longSelectionLength);
	}

	afterSelection() {
		clearTimeout(this.skipTimer);
		let that = this;
		this.forEachPlayer(function(playerName){
			let player = that.players[playerName];

			// check if a selection was made
			// we compare to undefined explicitly because the first card would evaluate to false because index 0
			//if(player.selection.includes(undefined) || (that.phase == "voting" && player.selection.length != 1) || (that.phase == "selection" && player.selection.length < that.currentBlack.answers)){
			if(player.selection.length == 0){
				// if the player didn't select anything, mark them inactive
				that.setPlayerActiveState(playerName, false);
				console.log(`${playerName} didn't select a card, marking inactive`);
			}
			else {

				// Update discard analytics
				for(let i = 0; i < player.discards.length; i++){
					Analytics.updateCard({text:player.hand[player.discards[i]].text, discards:1});
				}


				let cardGroup = [];

				// copy each card played to the table, then queue it for discard from the player's hand
				for(let i = 0; i < that.currentBlack.answers; i++){
					let card = player.hand[player.selection[i]];
					card.playedBy = playerName;
					cardGroup.push(card);
					that.receiveDiscard(playerName, player.selection[i], true);
				}

				that.cardsOnTable.push(cardGroup);

			}
		});

		this.waitingFor = [];
		this.clearSelections();
		this.processDiscards()
		this.votingPhase();
	}

	votingPhase() {
		let that = this;

		// if nobody selected anything, go to the next selection phase instead of voting on nothing
		if(this.cardsOnTable.length == 0){
			this.selectionPhase();
			return;
		}

		this.setPhase("voting");

		this.io.to(this.roomID).emit("update phase", this.phase);
		this.io.to(this.roomID).emit("countdown", ((this.currentBlack.answers == 1) ? votingLength : longVotingLength), Date.now());

		// shuffle the cards on the table and then show them to everyone
		this.shuffle(this.cardsOnTable);
		this.io.to(this.roomID).emit("cards on table", this.cardsOnTable);

		this.forEachPlayer(function(playerName){
			if(that.players[playerName].active && playerName != "Rando Cardrissian"){
				that.waitingFor.push(playerName);
			}
		});

		this.waiting = true;

		this.countdown = setTimeout(this.afterVoting.bind(this), ((this.currentBlack.answers == 1) ? votingLength : longVotingLength));
		this.timeLeft = ((this.currentBlack.answers == 1) ? votingLength : longVotingLength);
	}

	afterVoting() {
		let that = this;

		// use an object initially for the votes because that's easier to record into
		let selections = {};
		for(let i = 0; i < this.cardsOnTable.length; i++){
			selections[i] = 0;
		}

		this.forEachPlayer(function(playerName){
			if(playerName == rando){
				return;
			}
			let player = that.players[playerName];
			if(player.selection.length > 0){
				selections[player.selection[0]]++;
			}
			else {
				// player didn't vote
				that.setPlayerActiveState(playerName, false);
				let index = -1;
				// figure out which card on the table is theirs
				for(let i = 0; i < that.cardsOnTable.length; i++){
					if(that.cardsOnTable[i][0].playedBy == playerName){
						index = i;
						break;
					}
				}
				// either subtract one vote from their card, or intialize it with -1 votes
				if(index > -1){
					selections[index]--;
				}
			}
		});

		// copy the votes into an array because that's easier to sort
		this.votes = [];
		Object.keys(selections).forEach(function(key){
			that.votes.push([key, selections[key]]);
		});

		this.votes.sort(function(a, b) {
			return a[1] - b[1]
		}).reverse();
		
		this.clearSelections();

		// give the players the results of the vote
		this.io.to(this.roomID).emit("voting results", this.votes);

		if(this.cardsOnTable.length < 1){
			// nobody put anything on the table, skip to next selection
			this.selectionPhase();
		}

		// if first and second place have the same amount of votes, then we have a tie
		else if(this.votes.length > 1 && this.votes[0][1] == this.votes[1][1]){
			this.tiePhase();
		}
		else {
			this.winnerPhase(this.votes[0][0]);
		}

		this.waitingFor = [];
	}

	tiePhase() {
		this.setPhase("tie");
		console.log(`${this.name} entered ${this.phase} phase`);

		this.io.to(this.roomID).emit("update phase", this.phase);
		this.io.to(this.roomID).emit("countdown", tieLength, Date.now());

		// As a future feature, players should be allowed to vote on who wins the tie

		this.io.to(this.roomID).emit('chat message', `There's a tie! A winner will be picked randomly.`);
		this.countdown = setTimeout(this.afterTie.bind(this), tieLength);
		this.timeLeft = (tieLength);
	}

	afterTie() {
		// the amount of votes needed to win equals the amount that first place has
		const toTie = this.votes[0][1];
		let contestants = [];
		// check which indexes are involved in the tie
		for(const vote of this.votes){
			if(vote[1] != toTie){
				break;
			}
			else{
				contestants.push(vote[0]);
			}
		}

		// select a random index to be winner
		let winningIndex = contestants[Math.floor(Math.random() * (contestants.length))][0];
		this.io.to(this.roomID).emit("tie broken", winningIndex);

		this.clearSelections();
		this.winnerPhase(winningIndex);
	}

	winnerPhase(winningIndex) {

		// get the player who played the winning card
		if(this.cardsOnTable.length > 0){
			let winningPlayer = this.players[this.cardsOnTable[winningIndex][0].playedBy];

			// tell everyone which hand and player won
			this.io.to(this.roomID).emit('round winner', winningIndex, winningPlayer.name);

			// give the player a point
			winningPlayer.points++;

			// update their score in the global database too, if there were at least 3 options
			if(this.cardsOnTable.length > 2 && !winningPlayer.isGuest) {
				mongoUser.findOne({name:winningPlayer.name}, function(err, playerdb) {
					if(playerdb){
						playerdb.rounds_won++;
						playerdb.save();
					}
				});
			}

			// move this into the above code block after debugging
			if(this.currentBlack.answers == 1){
				for(let i = 0; i < this.cardsOnTable.length; i++){
					if(i == winningIndex){
						Analytics.updateCard({text:this.cardsOnTable[winningIndex][0].text, wins:1});
					}
					else {
						Analytics.updateCard({text:this.cardsOnTable[i][0].text, losses:1});
					}
				}
			}

			this.setPhase("winner");
			console.log(`${this.name} entered ${this.phase} phase`);
			this.io.to(this.roomID).emit("update phase", this.phase);
			this.io.to(this.roomID).emit("countdown", winLength, Date.now());

			this.io.to(this.roomID).emit('chat message', `<span class="player-name ${winningPlayer.color}">${winningPlayer.name}</span> won the round!`, undefined, true);
			this.countdown = setTimeout(this.afterWinner.bind(this, winningPlayer), winLength);
			this.timeLeft = (winLength);
		}
		else{
			// nobody put anything on the table, skip to next selection
			this.selectionPhase();
		}
	}

	afterWinner(winningPlayer) {
		this.discardTable()
		
		// check if the player won the game
		if(winningPlayer.points == this.settings.pointsToWin){
			this.gameOver(winningPlayer);
		}
		else{
			this.selectionPhase();
		}
	}

	gameOver(winningPlayer) {
		let that = this;

		this.setPhase("gameOver");
		this.io.to(this.roomID).emit('chat message', `<span class="player-name ${winningPlayer.color}">${winningPlayer.name}</span> won the game!<br/>They have a few seconds to gloat and then a new round will start automatically.`, undefined, true);
		this.io.to(this.roomID).emit('game winner', winningPlayer.name);

		this.io.to(this.roomID).emit("update phase", this.phase);
		this.io.to(this.roomID).emit("countdown", gameOverLength, Date.now());

		// update the user's score in the database
		if(this.settings.pointsToWin >= GameManager.pointsForWinToCount && !winningPlayer.isGuest){
			mongoUser.findOne({name:winningPlayer.name}, function(err, playerdb) {
				if(playerdb){
					playerdb.games_won++;
					playerdb.save();
				}
			});
		}

		this.forEachPlayer(function(playerName){
			let player = that.players[playerName];
			player.hand = [];
			player.points = 0;
		});

		this.round++;

		// start a new round at this point
		this.countdown = setTimeout(this.start.bind(this), gameOverLength);
		this.timeLeft = gameOverLength;
	}

	receiveSelection(playerName, indexes) {
		// indexes is formatted as an array up to 3-long, where the position corresponds to the position they're picking for (i.e. index 1 is the second answer of a question)
		// the value at that index is the index of card they're choosing for that
		// I could probably write that better
		
		//
		// Selection validation
		// Most of these shouldn't even be possible unless the user is messing with local scripts, so we'll just pretend we didn't hear them
		//

		try {
			if(!indexes){
				console.log(`WARNING: ${playerName} attempted to submit an empty selection`);
				return;
			}

			if(!Array.isArray(indexes)){
				console.log(`WARNING: ${playerName} attempted to submit a selection that is not an array`);
				return;
			}

			for(const index of indexes){
				if(index < 0){
					console.log(`WARNING: ${playerName} attempted to submit a selection of ${index}, less than zero`);
					return;
				}
				else if(!Number.isInteger(parseInt(index))){
					console.log(`WARNING: ${playerName} attempted to submit a selection that included "${index}", a non-integer value`);
					return;
				}
			}

			if(this.phase == "winner"){
				console.log(`WARNING: ${playerName} attempted to make a selection during the winner phase`);
				return;
			}

			// error checking exclusively for the voting phase
			else if(this.phase == "voting"){
				if(indexes.length != 1){
					// only accept an array of length 1
					console.log(`WARNING: ${playerName} attempted to submit a selection of length ${indexes.length} during the voting phase`);
					return;
				}
				else if(indexes[0] > this.cardsOnTable.length-1){
						console.log(`WARNING: ${playerName} attempted to submit a selection of ${indexes[0]}, greater than the max index of the table`);
						return;
				}
				else if(this.cardsOnTable[indexes[0]][0].playedBy == playerName && Object.keys(this.players).length > 2){
					console.log(`WARNING: ${playerName} attempted to vote for their own selection`);
					// don't let the player vote for their own card, unless the game has less than 2 players.
					return;
				}
			}
			else if (this.phase == "selection"){
				if(indexes.length != this.currentBlack.answers){
					console.log(`WARNING: ${playerName} attempted to submit a selection of length ${indexes.length}, which isn't equal to the number of answers expected (${this.currentBlack.answers})`);
					return;
				}
				let dupeChecker = []
				for(const index of indexes){
					if(index > this.players[playerName].hand.length-1){
						console.log(`WARNING: ${playerName} attempted to submit a selection of ${index}, greater than the max index of their hand`);
						return;
					}
					if(dupeChecker.includes(index)){
						console.log(`WARNING: ${playerName} attempted to submit duplicate cards of index ${index}`);
						return;
					}
					else {
						dupeChecker.push(index);
					}
				}
			}

			// don't accept changes to selection during the tie phase
			// someday there will be tiebreaking functionality and this can be restructured
			if(this.phase == "tie"){
				return;
			}
		}
		catch (error){
			console.log(`WARNING: Unexpected error occurred while attempting to validate selection of ${playerName} (but we caught it, don't worry). Error follows:`);
			console.log(error);
			console.log("Above error was caught successfully and is harmless.");
			return;
		}

		//
		// End selection validation
		//

		let player = this.players[playerName];

		player.selection = indexes;

		// if we're waiting for this player, stop doing so, and let everyone know
		if(this.waitingFor.includes(playerName)){
			this.waitingFor.splice(this.waitingFor.indexOf(playerName), 1);
			this.io.to(this.roomID).emit('player ready', playerName);
		}

		// if the player is inactive, mark them active and tell everyone they're back
		if(!player.active){
			if(!(this.waitingForSkip.includes(playerName)) && playerName != rando){
				this.waitingForSkip.push(playerName);
			}
			// it's important that they're added to the above list befor they're set active below,
			// otherwise the number of votes to skip will be incorrect
			this.setPlayerActiveState(playerName, true);
			this.io.to(this.roomID).emit('player ready', playerName);
		}

		// if we're not waiting for anybody else in selection or voting, advance to the next phase
		// Don't let Rando force this advance or we end up cycling rounds quickly
		if(this.waitingFor.length == 0 && playerName != "Rando Cardrissian"){
			if(this.phase == "selection"){
				if(this.waiting){
					clearTimeout(this.countdown);
					this.countdown = setTimeout(this.afterSelection.bind(this), 2000);
				}
				this.waiting = false;
			}
			else if(this.phase == "voting"){
				if(this.waiting){
					clearTimeout(this.countdown);
					this.countdown = setTimeout(this.afterVoting.bind(this), 2000);
				}
				this.waiting = false;
			}
		}

	}

	clearSelections() {
		let that = this;
		this.forEachPlayer(function(playerName){
			that.players[playerName].selection = [];
		});
	}

	receiveDiscard(playerName, index, toggle){
		//
		// Discard validation
		// Most of these shouldn't even be possible unless the user is messing with local scripts, so we'll just pretend we didn't hear them
		//
		try {
			if(index != 0){
				if(!index){
					console.log(`WARNING: ${playerName} attempted to submit an empty discard`);
					return;
				}
				else if(!parseInt(index)){
					console.log(`WARNING: ${playerName} attempted to submit a discard that did not parse to an int`);
					return;
				}
			}
			if(index < 0 || index > this.players[playerName].hand.length-1){
				console.log(`WARNING: ${playerName} attempted to submit a discard of ${index} which is outside the valid range`);
				return;
			}
		}
		catch (error){
			console.log(`WARNING: Unexpected error occurred while attempting to validate discard of ${playerName} (but we caught it, don't worry). Error follows:`);
			console.log(error);
			console.log("Above error was caught successfully and is harmless.");
			return;
		}
		//
		// End discard validation
		//

		let currentDiscards = this.players[playerName].discards;

		// if toggle is true, add the card to their discards if it isn't already there
		if(toggle == true && !(currentDiscards.includes(index))){
			currentDiscards.push(index);
		}

		// if toggle is false, remove the card from their discards if it's in there
		else if(toggle == false && currentDiscards.includes(index)){
			currentDiscards.splice(currentDiscards.indexOf(index), 1)
		}
	}

	processDiscards(){
		let that = this;
		this.forEachPlayer(function(playerName){
			let player = that.players[playerName];
			if(player.discards.length > 0){
				// remove each card by index from their hand and put it into the discard pile
				// we do this in sorted reverse so that the indexes of the remaining hand aren't affected
				player.discards.sort().reverse().forEach(function(index){
					that.whiteDiscards.push(player.hand.splice(index, 1)[0]);
				});
				that.io.to(player.socket.id).emit('remove white', player.discards);
				player.discards = [];
			}
		});
	}

	discardTable(){	
		// move the current black card to the discard pile and clear the table
		// the white cards are being discarded through processDiscards, which gets the cards on the table through afterSelection
		this.blackDiscards.push(this.currentBlack);
		this.cardsOnTable = [];
	}

	processMessage(playerName, message){
		let that = this;
		let player = that.players[playerName];

		if(message.length > GameManager.chatCharacterLimit){
			that.io.to(player.socket.id).emit('chat message', `Your message was over the character limit of ${GameManager.chatCharacterLimit}`);
			return;
		}
		
		// send the message to everyone
		this.io.to(this.roomID).emit('chat message', message, {name:playerName, color:player.color});

		// check if the message is adding a custom answer
		if(message.startsWith("-a ")){
			let new_card = message.substring(3);

			// Add it to the deck if there are enough cards to guarantee it won't be drawn in the next hand, otherwise add it to discards
			// We do this so the player has a longer grace period to undo their addition
			// the +2 is because on 3-answer questions, the max hand size goes up by 2.
			let minIndex = Object.keys(that.players).length * (that.settings.handSize + 2);
			let maxIndex = that.whiteCards.length;
			if(maxIndex > minIndex){
				// insert it into the deck at a random index between the minIndex and maxIndex
				let index = Math.floor((Math.random() * (maxIndex - minIndex)) + minIndex);
				that.whiteCards.splice(index, 0, {text:new_card, owner:player.name});
			}
			else {
				that.whiteDiscards.push({text:new_card, owner:player.name});
			}

			// Add it to the player's deck in the database
			if(player.isGuest){
				that.io.to(player.socket.id).emit('chat message', `Answer card added to current game: ${new_card}. Create an account to add it to your personal deck.`);
			}
			else {
				mongoUser.findOne({name:playerName}, function(err, playerdb) {
					playerdb.answer_deck.push(new_card);
					playerdb.save();

					player.lastCustom = new_card;

					that.io.to(player.socket.id).emit('chat message', `Answer card added to your deck: ${new_card}`);
				});
			}
		}

		// check if the message is an undo
		else if(message == "-undo"){
			// The player can only undo the last card they made, 1 layer deep. We'll attempt to remove it from both the active game, and the database

			if(player.lastCustom){
				let custom = player.lastCustom;
				let deck, discards;
				if(custom.answers){
					// if it has answers, it's a black card, so we search black decks
					deck = this.blackCards;
					discards = this.blackDiscards;
				}
				else {
					// otherwise it's white, so we search white decks, and make an object to make the search easier
					deck = this.whiteCards;
					discards = this.whiteDiscards;
					custom = {text:custom};
				}
				custom.owner = player.name;
		
				let found = false;
				// look through the deck for it
				for(let i = 0; i < deck.length; i++){
					let card = deck[i];
					// note that using .answers won't cause an error for white cards, because it will be undefined == undefined, which is true
					if(card.text == custom.text && card.owner == custom.owner && card.answers == custom.answers){
						deck.splice(i, 1);
						found = true;
						break;
					}
				}
				// if we didn't find it, look through the discards
				if(!found){
					for(let i = 0; i < discards.length; i++){
						let card = discards[i];
						if(card.text == custom.text && card.owner == custom.owner && card.answers == custom.answers){
							discards.splice(i, 1);
							found = true;
							break;
						}
					}
				}
				// if the card wasn't found above, it (should) mean it's currently in someone's hand. Whatever, it will go away after the game.

				// then delete it from the database. This happens whether we found the card in the current game or not.
				mongoUser.findOne({name:playerName}, function(err, playerdb) {
					// we use oldText solely so we can give the user confirmation of what we got rid of.
					let oldText = "";
					if(playerdb){

						// if it has an answers property, it's a question
						if(player.lastCustom.answers){
							oldText = playerdb.question_deck.pop().text;
						}
						// otherwise it's an answer
						else {
							oldText = playerdb.answer_deck.pop();
						}
						playerdb.save();
					}
					player.lastCustom = undefined;
					that.io.to(player.socket.id).emit('chat message', `Removed your last custom card: ${oldText}`);
				});
			}
		}

		// check if the message is asking for a roll, random integer from 1-100
		else if(message == "-roll"){
			that.io.to(that.roomID).emit('chat message', `Rolled a ${Math.floor(Math.random() * 100) + 1}`);
		}

		// check if the message is adding a custom question
		// we do this one last because the regex is a touch more expensive and questions are the least-used command (short of nuking)
		else if(message.match(/^-q [1-3] /)){
			let new_card = {
				text:message.substring(5),
				answers:parseInt(message[3]),
			}
			let minIndex = 1;
			let maxIndex = that.blackCards.length;
			if(maxIndex > minIndex){
				// insert it into the deck at a random index between the minIndex and maxIndex
				let index = Math.floor((Math.random() * (maxIndex - minIndex)) + minIndex);
				that.blackCards.splice(index, 0, {text:new_card.text, answers:new_card.answers, owner:player.name});
			}
			else {
				that.blackDiscards.push(new_card);
			}
			if(player.isGuest){
				that.io.to(player.socket.id).emit('chat message', `Question card with ${new_card.answers} added to current game: ${new_card.text}. Create an account to add it to your personal deck.`);
			}
			else{
				mongoUser.findOne({name:playerName}, function(err, playerdb) {
					playerdb.question_deck.push(new_card);
					playerdb.save();

					player.lastCustom = new_card;

					that.io.to(player.socket.id).emit('chat message', `Question card with ${new_card.answers} answers added to your deck: ${new_card.text}`);
				});
			}
		}
	}

	receiveSkip(playerName, toggle){
		//
		// Skip validation
		// Most of these shouldn't even be possible unless the user is messing with local scripts, so we'll just pretend we didn't hear them
		//
		if(this.phase != "selection"){
			return;
		}
		if(typeof(toggle) != "boolean"){
			try{
				toggle = Boolean(toggle);
			}
			catch {
				console.log(`WARNING: Unexpected error occurred while attempting to validate skip of ${playerName} (but we caught it, don't worry). Error follows:`);
				console.log(error);
				console.log("Above error was caught successfully and is harmless.");
				return;
			}
		}
		//
		// End skip validation
		//

		// if the bool is false, they're unskipping, so remove the countdown if there is one
		// and re-add the player to the list of players who haven't skipped
		if(!toggle){
			clearTimeout(this.skipTimer);
			if(!(this.waitingForSkip.includes(playerName))) {
				this.waitingForSkip.push(playerName);
			}
		}
		else {
			// if we're waiting for this player, stop doing so, and let everyone know
			if(this.waitingForSkip.includes(playerName)){
				this.waitingForSkip.splice(this.waitingForSkip.indexOf(playerName), 1);
			}
		}


		let player = this.players[playerName];

		// if the player is inactive, mark them active and tell everyone they're back
		if(!player.active){
			this.setPlayerActiveState(playerName, true);
		}

		this.calculateVotesToSkip();

		this.io.to(this.roomID).emit('skip counter', this.currentVotesToSkip, this.votesNeededToSkip);

		this.checkForSkip();
	}

	calculateVotesToSkip(){
		this.currentVotesToSkip = this.numActivePlayers - this.waitingForSkip.length;
		this.votesNeededToSkip = Math.max(Math.ceil(this.numActivePlayers * (2/3), 1));
	}

	checkForSkip(){
		// if two thirds of players have voted to skip, skip
		if(this.phase == "selection"){
			if(this.currentVotesToSkip >= this.votesNeededToSkip && this.currentVotesToSkip > 0 && !this.skipTimer) {
				this.skipTimer = setTimeout(function() {
					clearTimeout(this.countdown);
					if(this.settings.randoCardrissian){
						this.players[rando].selection = [];
						this.players[rando].hand = [];
						this.players[rando].active = false;
					}
					this.io.to(this.roomID).emit('chat message', "Question was skipped by vote");
					this.selectionPhase();
					this.skipTimer = null;
				}.bind(this), 2000);
			}
		}
	}

	setPhase(newPhase) {
		this.phase = newPhase;
	}

	addPlayer(playerName, socket) {
		let that = this;
		let isGuest = playerName.startsWith('~');
		mongoUser.findOne({name:playerName}, function(err, playerdb) {
			// if their preferred color is available, give them that. Otherwise give them the first available color
			let color = (!isGuest && that.availableColors.includes(playerdb.preferredColor) ? playerdb.preferredColor : that.availableColors[0]);

			let new_player = new Player(playerName, color, socket, playerdb, isGuest);

			// remove their color from the list of available colors
			that.availableColors.splice(that.availableColors.indexOf(color),1);

			that.players[playerName] = new_player;
			that.playerQueue.push(playerName);

			// provide the sender with a list of players already in the game (which should include themself)
			// that.io.to(socket.id).emit('player list', Object.keys(that.players));
			that.distributePlayerList(new_player.socket.id);

			// Share the current settings with the new player
			that.io.to(new_player.socket.id).emit('update settings display', {
				goal: that.settings.pointsToWin,
				hand: that.settings.handSize,
				rando: that.settings.randoCardrissian,
				global: that.settings.globalCustoms,
				base: that.settings.decksEnabled.base,
				green: that.settings.decksEnabled.green,
				blue: that.settings.decksEnabled.blue
			});
			
			// count how many customs they have and let everyone know
			if(new_player.customAnswerCount > 0 || new_player.customQuestionCount > 0){
				that.localCustomAnswerCount += new_player.customAnswerCount;
				that.localCustomQuestionCount += new_player.customQuestionCount;

				that.io.to(that.roomID).emit('update local custom counts', {
					answers: that.localCustomAnswerCount,
					questions: that.localCustomQuestionCount,
				});
			}

			// tell everyone in the room except the new player that they've joined
			new_player.socket.to(that.roomID).emit('player join', {
				name: new_player.name,
				color: new_player.color,
				lifetimePoints: new_player.lifetimePoints,
				lifetimeWins: new_player.lifetimeWins,
			});
		});
	}

	distributePlayerList(socketid) {
		let that = this;
		let playerList = [];
		this.forEachPlayer(function(playerName){
			let player = that.players[playerName];
			playerList.push({
				name: playerName,
				color: player.color,
				points: player.points,
				lifetimePoints: player.lifetimePoints,
				lifetimeWins: player.lifetimeWins,
				inGame : player.inGame,
			});
		}, true);
		if(socketid){
			this.io.to(socketid).emit('distribute players', playerList);
		}
		else{
			this.io.to(this.roomID).emit('distribute players', playerList);
		}
	}

	removePlayer(playerName) {
		let player = this.players[playerName];

		// tell everyone they left
		this.io.to(this.roomID).emit('chat message', `<span class="player-name ${player.color}">${playerName}</span> left the game`, undefined, true);
		this.io.to(this.roomID).emit('player leave', {
			name:player.name,
			color:player.color,
		});

		//	delete them from the list of players, but only in the lobby
		if(this.phase == "lobby"){
			// make their color available again
			this.availableColors.push(player.color);
			this.localCustomAnswerCount -= player.customAnswerCount;
			this.localCustomQuestionCount -= player.customQuestionCount;
			this.io.to(this.roomID).emit('update local custom counts', {
				answers: this.localCustomAnswerCount,
				questions: this.localCustomQuestionCount,
			});
			delete this.players[playerName];
			this.playerQueue.splice(this.playerQueue.indexOf(playerName), 1);
		}
		else if(this.phase != "transition"){
			player.inGame = false;
			if(player.active){
				player.active = false;
				this.numActivePlayers -= 1;
			}

			// stop waiting for their selection (for game)
			if(this.waitingFor.includes(playerName)){
				this.waitingFor.splice(this.waitingFor.indexOf(playerName), 1);
			}
			if(this.waitingForSkip.includes(playerName)){
				this.waitingForSkip.splice(this.waitingForSkip.indexOf(playerName), 1);
			}
			// Check if we can skip now
			this.calculateVotesToSkip();
			this.io.to(this.roomID).emit('skip counter', this.currentVotesToSkip, this.votesNeededToSkip);
			this.checkForSkip();
		}
		
		if(this.checkForEmptyGame()){this.startSelfDestruct()};
	}

	rejoinPlayer(playerName, socket){
		if(this.includesPlayer(playerName)){
			let player = this.players[playerName];

			if(!player.inGame){
				clearTimeout(this.selfDestructTimer);
				player.inGame = true;
				player.socket = socket;
				if(!(this.waitingFor.includes(playerName))){
					this.waitingFor.push(playerName);
				}
				if(!(this.waitingForSkip.includes(playerName))){
					this.waitingForSkip.push(playerName);
				}
				this.setPlayerActiveState(playerName, true);

				player.socket.to(this.roomID).emit('player rejoin', playerName);
				this.io.to(this.roomID).emit('chat message', `<span class="player-name ${player.color}">${playerName}</span> rejoined the game`, undefined, true);
				this.io.to(player.socket.id).emit('game info', {
					playerName: playerName,
					gameName: this.name,
					playerCount: Object.keys(this.players).length
				});
				this.distributePlayerList(player.socket.id);
				this.gatherWhite(playerName);
				let handToSend = [];
				for(const card of player.hand){
					handToSend.push([card]);
				}
				let rejoinGameState = {phase:this.phase, hand:handToSend, cardsOnTable:this.cardsOnTable};
				this.io.to(player.socket.id).emit('rejoin game state', rejoinGameState);
				this.io.to(player.socket.id).emit('distribute black', this.currentBlack);
				this.io.to(player.socket.id).emit('update phase', this.phase);
				this.io.to(player.socket.id).emit('countdown', this.timeLeft, Date.now());
				this.io.to(player.socket.id).emit('chat message', "You just rejoined the game. The game should still be functional but some stuff might look weird, that should clear up as phases change.");
				// the value of the countdown timer isn't stored server-side so we can't access that
			}
		}
	}

	checkForEmptyGame(){
		let empty = true;
		for(const player in this.players){
			if(this.players[player].inGame && player != rando){
				empty = false;
				break;
			}
		}
		if(empty) return true;
		else return false;
	}

	startSelfDestruct(){
		console.log(`${this.name} starting self destruct sequence`);
		this.selfDestructTimer = setTimeout(function(){
			if(this.checkForEmptyGame()){
				console.log(`${this.name} deleting self because it is empty`);
				this.deleteSelf();
			}
			else {
				clearTimeout(this.selfDestructTimer);
				console.log(`${this.name} aborting self destruct`);
			}
		}.bind(this), selfDestructLength);
	}

	updateSettings(settingsDict){
		if(this.phase != "lobby"){
			return
		}
		let toSend = {};
		const clamp = (num, min, max) => Math.min(Math.max(num, min), max);
		for(const setting in settingsDict){
			let value = settingsDict[setting];
			switch (setting){
				case "goal":
					if(Number.isInteger(parseInt(value))){
						this.settings.pointsToWin = clamp(parseInt(value), GameManager.minPoints, GameManager.maxPoints);
					}
					toSend[setting] = value;
					break;
				case "hand":
					if(Number.isInteger(parseInt(value))){
						this.settings.handSize = clamp(parseInt(value), GameManager.minHandSize, GameManager.maxHandSize);
					}
					toSend[setting] = value;
					break;
				case "rando":
					if(typeof(value) === "boolean"){
						this.settings.randoCardrissian = value;
					}
					toSend[setting] = value;
					break;
				case "global":
					if(typeof(value) === "boolean"){
						this.settings.globalCustoms = value;
					}
					toSend[setting] = value;
					break;
				default:
					// check the decks
					if(GameManager.standardDecks.hasOwnProperty(setting)){
						if(typeof(value) === "boolean"){
							this.settings.decksEnabled[setting] = value;
						}
						toSend[setting] = value;
						break;
					}
			}
		}
		if(Object.keys(toSend).length > 0){
			this.players[this.getHost()].socket.to(this.roomID).emit('update settings display', toSend);
		}
	}

	updatePlayerSocket(name, socket) {
		this.players[name].socket = socket;
	}

	playerReconnect(name, socket) {
		this.updatePlayerSocket(name, socket);
		this.players[name].inGame = true;
		if(this.phase == "transition" && this.waitingFor.includes(name)){
			this.waitingFor.splice(this.waitingFor.indexOf(name), 1);
		}
	}

	changePlayerColor(name, color) {
		// don't do anything if this isn't an approved color
		if(colors.includes(color)) {

			// make sure the color isn't already taken
			if(this.availableColors.includes(color)){
				// move their old color back into the array of available colors
				let oldColor = this.players[name].color;
				this.availableColors.push(oldColor);

				// take their new color out of the array of available colors
				this.players[name].color = color;
				this.availableColors.splice(this.availableColors.indexOf(color), 1);

				// tell everyone they changed color
				this.io.to(this.roomID).emit('update player color', name, color, oldColor);
			}
		}
	}

	shuffleAll() {
		console.log("shuffling");
		this.shuffle(this.whiteCards);
		this.shuffle(this.blackCards);
		console.log("done shuffling");
	}

	shuffle(deck) {
		// fisher-yates shuffle, copied from https://javascript.info/task/shuffle
		for (let i = deck.length - 1; i > 0; i--) {
			let j = Math.floor(Math.random() * (i + 1)); // random index from 0 to i

			// swap elements array[i] and array[j]
			[deck[i], deck[j]] = [deck[j], deck[i]];
		}
	}

	drawWhite() {
		// if the draw pile is empty, shuffle the discards back into it
		if(this.whiteCards.length == 0){
			console.log("White deck is empty, shuffling discards.")
			this.whiteCards = [...this.whiteDiscards];
			this.whiteDiscards = [];
			this.shuffle(this.whiteCards);
		}
		let card = this.whiteCards.pop();
		card.text = this.processMagicWords(card.text);
		return card;
	}

	drawBlack() {
		// if the draw pile is empty, shuffle the discards back into it
		if(this.blackCards.length == 0){
			console.log("Black deck is empty, shuffling discards.")
			this.blackCards = [...this.blackDiscards];
			this.blackDiscards = [];
			this.shuffle(this.blackCards);
		}
		let card = this.blackCards.pop();
		card.text = this.processMagicWords(card.text);
		return card;
	}

	gatherWhite(playerName){
		let new_cards = [];
		let player = this.players[playerName];
		// Give them a card until they're up to the max hand size. The max hand size increases by 1 for 2-answer questions, and by 2 for 3-answer questions
		for(let i = this.players[playerName].hand.length; i < (this.settings.handSize + (this.currentBlack.answers - 1)); i++){
			let card = this.drawWhite();
			// if they draw a card from someone they've banned, discard it and draw another
			while(player.bannedAuthors.includes(card.owner)){
				this.whiteDiscards.push(card);
				card = this.drawWhite();
			}
			new_cards.push([card]);
			player.hand.push(card)
		}
		return new_cards;
	}

	distributeWhite(playerName) {
		if(playerName == "Rando Cardrissian"){
			return; // No cards for rando :(
		}
		let player = this.players[playerName];
		let new_cards = this.gatherWhite(playerName);

		this.io.to(player.socket.id).emit('distribute white', new_cards);
	}

	forEachPlayer(func, all=false){
		// execute a function for each player
		// if all is false, only players still in the game with be affected
		let that = this;
		Object.keys(this.players).forEach(function(playerName){
			if(all || that.players[playerName].inGame){
				func(playerName);
			}
		});
	}

	getHost() {
		return this.playerQueue[0];
	}

	isHost(playerName) {
		return this.playerQueue[0] == playerName;
	}

	includesPlayer(playerName){
		return this.playerQueue.includes(playerName);
	}

	playerIsGone(playerName){
		return (this.includesPlayer(playerName) && !this.players[playerName].inGame);
	}

	setPlayerActiveState(playerName, state){
		if(playerName in this.players && typeof(state) == "boolean"){
			let player = this.players[playerName]
			if(player.active == state){
				return false;
			}
			else {
				player.active = state;
				if(state){
					this.numActivePlayers += (playerName == rando) ? 0 : 1;
					this.io.to(this.roomID).emit('player active', playerName);
					this.calculateVotesToSkip();
					this.io.to(this.roomID).emit('skip counter', this.currentVotesToSkip, this.votesNeededToSkip);
					this.checkForSkip();
				}
				else {
					this.numActivePlayers -= (playerName == rando) ? 0 : 1;
					this.io.to(this.roomID).emit('player inactive', playerName);
					// We don't need to do the skip counter here because the phase changes handle it
				}
			}
		}
		else {
			return false;
		}
	}

	// this needs to be async but I'm not entirely sure why
	async deleteSelf() {
		// our active timeout must be cleared for the garbage collector to take the instance
		clearTimeout(this.selfDestructTimer);
		clearTimeout(this.countdown);
		clearTimeout(this.countdownInterval);
		clearTimeout(this.skipTimer);
		// we use this negative round if everyone mysteriously disappears during the transition
		this.round = -1;
		// delete the game from the mongo database
		await mongoGame.deleteOne({_id:this.roomID});
		// tell the server to remove the game from the list of games
		eventEmitter.emit('delete room', this.roomID);
	}

}

module.exports = GameManager;