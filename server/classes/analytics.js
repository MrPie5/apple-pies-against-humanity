const mongoCardAnalytics = require("../mongodb_models/card-analytics_model.js");

class Analytics {
    static updateCard(card){
        if(!card.text) {
            return false;
        }

        if(!card.text) {
            return false;
        }
        mongoCardAnalytics.findOne({text:card.text}, function(err, analyticsdb) {
            if(err){
                console.log(error);
                return false;
            }
            if(analyticsdb){
                analyticsdb.wins += (card.wins) ? card.wins : 0;
                analyticsdb.losses += (card.losses) ? card.losses : 0;
                analyticsdb.discards += (card.discards) ? card.discards : 0;
                analyticsdb.save();
            }
            else {
                const newCard = new mongoCardAnalytics({
                    text: card.text,
                    wins: (card.wins) ? card.wins : 0,
                    losses: (card.losses) ? card.losses : 0,
                    discards: (card.discards) ? card.discards : 0,
                });
                newCard.save();
            }
        });
    }

    //     try {
    //         if(typeof(card.text) != "object"){
    //             card.text = [card.text];
    //         }
    //         if(typeof(card.text[0] != "string")) {
    //             return false;
    //         }
    //     }
    //     catch {
    //         return false;
    //     }

    //     mongoCardAnalytics.find({text: {$in:card.text}}, function(err, analytics) {
    //         if(err){
    //             console.log(error);
    //             return false;
    //         }
    //         analytics.forEach(function(doc){
    //             if(analyticsdb){
    //                 analyticsdb.wins += (card.wins) ? card.wins : 0;
    //                 analyticsdb.losses += (card.losses) ? card.losses : 0;
    //                 analyticsdb.discards += (card.discards) ? card.discards : 0;
    //             }
    //             else {
    //                 const newCard = new mongoCardAnalytics({
    //                     text: card.text,
    //                     wins: (card.wins) ? card.wins : 0,
    //                     losses: (card.losses) ? card.losses : 0,
    //                     discards: (card.discards) ? card.discards : 0,
    //                 });
    //                 newCard.save();
    //             }
    //         })
    //     });
    // }
}

module.exports = Analytics;