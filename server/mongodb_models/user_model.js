const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('user', new Schema({
    //Name of User
    name: {
      type: String,
      required: true
    },

    //Email of User
    email: {
      type: String,
      default: ""
    },

    //Hashed Password of User
    password: {
      type: String,
      required: true
    },

    // preferred color to be
    preferredColor: {
      type: String,
      default: ""
    },

    // preferred volume to start at
    volume: {
      type: Number,
      default: 1
    },

    banned_authors: {
      type: [String],
      default: []
    },

    //user's custom deck
    answer_deck: {
      type: [String],
      default: []
    },

    question_deck: {
      type: [Object],
      default: []
    },

    rounds_won: {
      type: Number,
      default: 0
    },

    games_won: {
      type: Number,
      default: 0
    },
}));