const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('game', new Schema({
    //room name
    name: {
      type: String,
      required: true
    },

    //array of user names in the game
    players: {
      type: [String],
      required: true
    },

    phase: {
      type: String,
      required: true
    },
    
    //password for the game: This gets hashed
    password: {
      type: String,
      required: false
    },

    globalShuffle: {
      type:Boolean,
      default: false
    },

    randoCardrissian:{
      type:Boolean,
      default: false
    },
}));
