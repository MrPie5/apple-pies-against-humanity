const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('card-analytics', new Schema({
    //room name
    text: {
        type: String,
        required: true
    },

    //owner of the card
    // owner: {
    //     type: String,
    //     required: true
    // },

    // how many times the card has won a round
    wins: {
        type: Number,
        default: 0
    },
    
    // how many times the card has been played in a round, but not won
    losses: {
        type: Number,
        default: 0
    },

    // how many times the card has been discarded from a player's hand
    discards: {
        type: Number,
        default: 0
    },
}));
