const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = mongoose.model('token', new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "user",
  },

  token: {
    type: String,
    required: true,
  },

  createdAt: {
    type: Date,
    default: Date.now,
    expires: 900,// this is the expiry time in seconds
  },
}));
