const jwt = require('jsonwebtoken');
const secret = require("../config.js").secret;

class Authentication {
    static authenticate(redirectOnFailure = true, allowGuests = true) {
        return function(req, res, next) {
            jwt.verify(req.cookies.token, secret, function(err, loggedIn) {
                if(err || !loggedIn){
                    if(redirectOnFailure){
                        return res.status(401).clearCookie('token').cookie('target', req.url).redirect('/');
                    }
                    else {
                        req.loggedIn = false;
                    }
                }
                else if(loggedIn.isGuest && !allowGuests){
                    if(redirectOnFailure){
                        return res.status(403).redirect('/');
                    }
                    else {
                        return res.status(403);
                    }
                }
                else {
                    req.loggedIn = loggedIn;
                }
                next();
            });
        }
    }

}

module.exports = Authentication;