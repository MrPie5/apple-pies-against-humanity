const express = require("express");
const app = express();
const router = express.Router();
const mongoUser = require("../mongodb_models/user_model.js");
const mongoToken = require("../mongodb_models/token_model.js");
const Authentication = require( "./authentication.js");
const authenticate = Authentication.authenticate;
const GameManager = require("../classes/gameManager.js");
const crypto = require("crypto");
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const config = require("../config.js");
app.set('secret', config.secret);

const nameAdjectives = require("../name_parts/adjectives.js");
const nameNouns = require("../name_parts/nouns.js");

const maxNameLength = 32;
const maxEmailLength = 254;
const chatCharacterLimit = GameManager.chatCharacterLimit;
const minResponses = GameManager.minResponses;
const maxResponses = GameManager.maxResponses;

router.post("/register",  async function(req, res) {
    if(!req.body.username) {
        return res.status(400).send(`Registration failed. You must provide a username you egg.`);
    }
    else if(req.body.username.length > maxNameLength) {
        return res.status(400).send(`Registration failed. Username greater than the max of ${maxNameLength} characters`);
    }
    else if(req.body.username.match(/^\s|\s$/)){
        return res.status(400).send(`Registration failed. Username can not start or end in whitespace.`);
    }
    else if(req.body.username.startsWith('~')){
        return res.status(400).send(`Registration failed. Username can not start with a \'~\', as this is reserved for guest accounts.`);
    }
    else if(req.body.email && req.body.email.length > maxEmailLength) {
        return res.status(400).send(`Registration failed. Email greater than the max of ${maxEmailLength} characters`);
    }
    else if(!req.body.password) {
        return res.status(400).send(`Registration failed. You must provide a password you walnut.`);
    }

    let userdb = await mongoUser.findOne({ name: req.body.username });

    if (userdb) {
        // if the user already exists, return an error message
        return res.status(400).send('Registration failed. Username already exists.');
    }
    else {

        // create new user
        const newUser = new mongoUser({
            name: req.body.username,
            email: req.body.email,
            password: req.body.password,
            rounds_won: 0,
            games_won: 0
        });

        newUser.password = await generatePasswordHash(newUser.password);

        var token = jwt.sign({username:newUser.name}, app.get('secret'),{expiresIn:'7d'});
        res.cookie('token',token,{httpOnly:true, expires:new Date(Date.now()+604800000)});

        newUser.save().then(
            res.send("Account created. If you are not redirected, refresh the page.")
        );
    }
});

router.post('/login', function(req, res) {
    // find the user
    mongoUser.findOne({name: req.body.username}, function(err, userdb) {

        if(err){
            return res.status(500).json({"message":"Unknown error occurred"});
        }

        if (!userdb) {
            res.status(404).json({"message":'Authentication failed. User not found.'});
        }
        else {
            // check if password matches
            bcrypt.compare(req.body.password,userdb.password, function(err, result) {
                if (err) {
                    console.log(err);
                    return res.status(500).send("An unknown error occurred, please try again.");
                }

                if (!result) {
                    res.status(400).json({"message":'Authentication failed. Wrong password.'});
                }
                else {
                    var token = jwt.sign({username: userdb.name}, app.get('secret'),{expiresIn:'7d'});
                    let target = "/menu"
                    if(req.cookies.target && req.cookies.target != "/"){
                        target = req.cookies.target;
                    }
                    res.cookie('token',token,{httpOnly:true, expires:new Date(Date.now()+604800000)}).clearCookie('target').json({"message":"Successfully logged in, if you are not redirected, refresh the page.", "target":target});
                }
            });
        }
    });
});

// this route is for sending the password reset email
router.post('/forgotPassword', async function(req, res) {
    // find the user
    const userdb = await mongoUser.findOne({name: req.body.username});

    if (!userdb) {
        return res.status(404).send('User not found.');
    }
    else {

        let token = await mongoToken.findOne({ userId: userdb._id });
        if (token) {
                if(Date.now() - Date.parse(token.createdAt) < 60000){
                    // rate limit resets to one per minute
                    return res.status(429).send("A password reset was recently requested.")
                }
                else{
                    await token.deleteOne()
                }
        }

        let resetToken = crypto.randomBytes(32).toString("hex");

        const hash = await bcrypt.hash(resetToken, Number(10));

        await new mongoToken({
            userId: userdb._id,
            token: hash,
            createdAt: Date.now(),
        }).save();

        const link = req.protocol + "://" + req.get('host') + `/user/resetPassword?token=${resetToken}&id=${userdb._id}`;

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: config.emailAddress,
                pass: config.emailPassword
            }
        });

        const mailOptions = {
            from: config.emailAddress,
            to: userdb.email,
            subject: 'Password reset - Apple Pies Against Humanity',
            text: `Hello ${userdb.name},\nYou requested to reset your password.\nClick the link below to reset your password:\n${link}`
        }

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
                res.status(500).send("An unexpected error occurred when attempting to send the email.");
            } else {
                res.send("An email has been sent to the address associated with this account");
            }
        });
    }
});

router.post('/loginAsGuest', function(req, res) {
    let name =
        // starting the name with a tilde is reserved for guest accounts only,
        // this ensures they won't conflict with real users.
        "~" +
        nameAdjectives[Math.floor(Math.random() * nameAdjectives.length)] +
        nameNouns[Math.floor(Math.random() * nameNouns.length)] +
        String(Math.floor(Math.random() * 1000));
        // 42,180,000 possible combinations
        // I'm not doing any checking for duplicate guest names, but the chances of a collision are so small
        // and the consequences so minor (can't join the same game) that I'm not going to worry about it

    let target = "/menu"
    if(req.cookies.target && req.cookies.target != "/"){
        target = req.cookies.target;
    }

    let token = jwt.sign({username:name,isGuest:true}, app.get('secret'),{expiresIn:'1d'});
    res.cookie('token',token,{httpOnly:true, expires:0}).clearCookie('target').json({"message":"Successfully logged in, if you are not redirected, refresh the page.", "target":target});;
});

// this route is for processing the actual password reset through the link provided through the above route
router.get('/resetPassword', async function(req, res) {
    const token = req.query.token;
    const id = req.query.id;

    let passwordResetToken = await mongoToken.findOne({ userId:id });
    if(!passwordResetToken){
        return res.redirect('/');
    }
    const isValid = await bcrypt.compare(token, passwordResetToken.token);
    if(!isValid){
        return res.redirect('/');
    }

    else {
        res.sendFile('/protected/resetPassword.html', {root:'.'});
    }
});

router.put('/reset', async function(req, res){
    const token = req.query.token;
    const id = req.query.id;

    let passwordResetToken = await mongoToken.findOne({ userId:id });
    if(!passwordResetToken){
        return res.status(401).send("Invalid or expired token. Please request a new reset link.");
    }
    const isValid = await bcrypt.compare(token, passwordResetToken.token);
    if(!isValid){
        return res.status(401).send("Invalid or expired token. Please request a new reset link.");
    }
    else {
        let userdb = await mongoUser.findOne({_id:id});
        if(!userdb){
            return res.status(404).send("Invalid user");
        }
        else{
            userdb.password = await generatePasswordHash(req.body.password);

            userdb.save().then( function(){
                passwordResetToken.deleteOne();
                res.send("Successfully changed password. You can now return to the login screen and log in.");
            });
        }
    }
});

router.put('/changeEmail', authenticate(redirectOnFailure=true,allowGuests=false), function(req, res){
    if(req.body.email.length > maxEmailLength){
        return res.status(400).send(`Email greater than the max of ${maxEmailLength} characters`);
    }
    mongoUser.findOne({name:req.loggedIn.username}, function(err, userdb){
        if(err){
            return res.status(500).send("Unknown error occurred");
        }
        if(!userdb){
            return res.status(404).send("User not found");
        }
        userdb.email = req.body.email;
        userdb.save();
        return res.status(200).send();
    });
});

router.get('/preferences', (req, res) => {
    // redirect to the preferences page without the /user/
    return res.redirect('/preferences');
});

router.get('/settings', (req, res) => {
    return res.redirect('/preferences');
});

router.get('/logout', function(req, res){
    res.clearCookie('token').redirect('/');
});

router.put('/setPreferredColor', authenticate(redirectOnFailure=true,allowGuests=false), function(req, res){
    mongoUser.findOne({name:req.loggedIn.username}, function(err, userdb){
        if(err){
            return res.status(500).send("Unknown error occurred");
        }
        if(!userdb){
            return res.status(404).send("User not found");
        }
        let color = req.body.color;
        if(!(["red", "blue", "green", "purple", "orange", "yellow", "lightblue", "pink"].includes(color))){
            return res.status(400).send("Invalid color")
        }
        else{
            userdb.preferredColor = color;
            userdb.save();
            return res.status(200).send();
        }
    });
});

router.get('/listAnswers', authenticate(redirectOnFailure=true,allowGuests=false), function(req, res){
    mongoUser.findOne({name:req.loggedIn.username}, function(err, userdb){
        if(!userdb){
            res.status(404).send("User not found");
        }
        res.send(userdb.answer_deck);
    });
});

router.put('/updateAnswers', authenticate(redirectOnFailure=true,allowGuests=false), function(req, res){
    mongoUser.findOne({name:req.loggedIn.username}, function(err, userdb){
        if(err){
            return res.status(500).json({"message":"Unknown error occurred"});
        }
        if(!userdb){
            return res.status(404).json({"message":"User not found"});
        }
        let message = "";
        let answers = {};

        try{
            answers = JSON.parse(req.body.answers);
            toDelete = [];
            for(const entry of Object.entries(answers)) {
                try {
                    const key = Number(entry[0]);
                    const value = String(entry[1]);
                    if(value.length > chatCharacterLimit){
                        console.log(`"${value}" is too long, skipping`)
                        message += `Skipping "${value}", entry is greater than the character limit of ${chatCharacterLimit}.\n`;
                        delete answers[key];
                        continue;
                    }
                    else if(value.length < 1){
                        toDelete.push(key);
                    }
                    else if(key < 0){
                        userdb.answer_deck.push(value);
                    }
                    else {
                        if(key < userdb.answer_deck.length){
                            userdb.answer_deck[key] = value;
                        }
                        else {
                            delete answers[key];
                            continue;
                        }
                    }
                }
                catch (error){
                    console.log(`skipped "${entry}"`)
                    console.log(error);
                    // skip this entry
                    delete answers[entry[0]]
                    continue;
                }
            }

            // Now delete the queued items
            // We do this in reverse so we don't offset the indexes every time we delete one
            for(const index of toDelete.reverse()){
                try{
                    userdb.answer_deck.splice(index, 1);
                }
                catch(error) {
                    console.log("Error deleting an entry. Error follows:");
                    console.log(error);
                    continue;
                }

            }
        }
        catch (error){
            switch (error){
            case TypeError:
                return res.status(400).json({"message":"Improperly formatted request"});
            default:
                console.log(`Unexpected error updating answer deck of ${req.loggedIn.username}. Error follows:`);
                console.log(error);
                return res.status(500).json({message:"An unexpected error occurred while updating your deck."});
            }
        }

        try{
            if(answers.length < 1){
                return res.status(400).json({"message":"All of the submitted changes were invalid or encountered an error."});
            }
            else {
                // Successfully updated
                userdb.markModified('answer_deck');
                userdb.save().then(function(){
                    res.status(200).json({"message":`${message}Deck updated`, "deck":userdb.answer_deck});
                })
            }
        }
        catch(error){
            console.log(`Unexpected error updating answer deck of ${req.loggedIn.username}. Error follows:`);
            console.log(error);
            return res.status(500).json({"message":"An unexpected error occurred while updating your deck."});
        }

    });
});

router.get('/listQuestions', authenticate(redirectOnFailure=true,allowGuests=false), function(req, res){
    mongoUser.findOne({name:req.loggedIn.username}, function(err, userdb){
        if(err){
            return res.status(500).send("Unknown error occurred");
        }
        if(!userdb){
            res.status(404).send("User not found");
        }
        res.send(userdb.question_deck);
    });
});

router.put('/updateQuestions', authenticate(redirectOnFailure=true,allowGuests=false), function(req, res){
    mongoUser.findOne({name:req.loggedIn.username}, function(err, userdb){
        if(err){
            return res.status(500).json({"message":"Unknown error occurred"});
        }
        if(!userdb){
            return res.status(404).json({"message":"User not found"});
        }
        let message = "";
        let questions = {};

        try{
            questions = JSON.parse(req.body.questions);
            toDelete = [];
            for(const entry of Object.entries(questions)) {
                try {
                    const key = Number(entry[0]);
                    const value = entry[1];
                    const responses = Number(value[0]);
                    const text = String(value[1]);
                    if(text.length > chatCharacterLimit){
                        console.log(`"${value}" is too long, skipping`)
                        message += `Skipping "${value}", entry is greater than the character limit of ${chatCharacterLimit}.\n`;
                        delete questions[key];
                        continue;
                    }
                    else if(text.length < 1){
                        toDelete.push(key);
                    }
                    else if(responses < minResponses || responses > maxResponses){
                        delete questions[key];
                        continue;
                    }
                    else if(key < 0){
                        userdb.question_deck.push({"text":text, "answers":responses});
                    }
                    else {
                        if(key < userdb.question_deck.length){
                            userdb.question_deck[key] = {"text":text, "answers":responses};
                        }
                        else {
                            console.log(`${key} is too large of an index, skipping`);
                            delete questions[key];
                            continue;
                        }
                    }
                }
                catch (error){
                    console.log(error);
                    console.log(`skipped "${entry}"`)
                    // skip this entry
                    delete questions[entry[0]]
                    continue;
                }
            }

            // Now delete the queued items
            // We do this in reverse so we don't offset the indexes every time we delete one
            for(const index of toDelete.reverse()){
                try{
                    const text = userdb.question_deck[index].text;
                    userdb.question_deck.splice(index, 1);
                }
                catch(error) {
                    console.log("Error deleting an entry. Error follows:");
                    console.log(error);
                    continue;
                }

            }
        }
        catch (error){
            switch (error){
            case TypeError:
                return res.status(400).json({"message":"Improperly formatted request"});
            default:
                console.log(`Unexpected error updating answer deck of ${req.loggedIn.username}. Error follows:`);
                console.log(error);
                return res.status(500).json({message:"An unexpected error occurred while updating your deck."});
            }
        }

        try{
            if(questions.length < 1){
                return res.status(400).json({"message":"All of the submitted changes were invalid or encountered an error."});
            }
            else {
                // Successfully updated
                userdb.markModified('question_deck');
                userdb.save().then(function(){
                    res.status(200).json({"message":`${message}Deck updated`, "deck":userdb.question_deck});
                })
            }
        }
        catch(error){
            console.log(`Unexpected error updating question deck of ${req.loggedIn.username}. Error follows:`);
            console.log(error);
            return res.status(500).json({"message":"An unexpected error occurred while updating your deck."});
        }

    });
});

async function generatePasswordHash(password){
    // generate a salt value
    let salt = await bcrypt.genSalt(10);
    // hash the plaintext password with the salt
    return bcrypt.hash(password, salt);
}

module.exports = router;