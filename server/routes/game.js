const express = require("express");
const app = express();
const router = express.Router();
const mongoGame = require("../mongodb_models/game_model.js");
const mongoUser = require("../mongodb_models/user_model.js");
const Authentication = require( "./authentication.js");
const authenticate = Authentication.authenticate;
const bcrypt = require("bcryptjs");
// const jwt    = require('jsonwebtoken');
const config =  require("../config.js");
const GameManager = require("../classes/gameManager.js");
app.set('secret', config.secret);

const maxRoomName = 64;

router.post("/create", authenticate(), async function(req, res) {
    if(req.body.roomName.length > maxRoomName){
        return res.status(400).send(`Game name is longer than the max of ${maxRoomName} characters`);
    }
    else if(req.body.roomName.length == 0){
        return res.status(400).send("You must provide a room name");
    }
    else if(req.body.roomName.match(/\s$/)){
        return res.status(400).send("Room name can not end in whitespace");
    }
    else if(req.body.roomName.match(/\\|\//)){
        return res.status(400).send("Room name can not contain slashes");
    }
    mongoGame.findOne({ name: req.body.roomName }, async function(err, room){
        if (room) {
            // return error message
            return res.status(400).send("Game name already exists");
        }
        else {
            let password = undefined;
            if(req.body.password){
                password = await generatePasswordHash(req.body.password)
            }

            // create new room
            const newGame = new mongoGame({
                name: req.body.roomName,
                players : [req.loggedIn.username],
                phase: 'lobby',
                password: password,
            });

            newGame
            .save()
            .then(res.send("Successfully joined"))
            .catch(err => console.log(err));
        }
	});
});

router.get("/list", (req, res) => {
    mongoGame.find({}, function(err, games){
        if(err){
            console.log(err);
            res.json(err);
        }
        else {
            let toSend = {"lobbies":[], "rejoin":[]}
            games.forEach(game => {
                if(game.phase == "lobby" && game.players.length < GameManager.maxPlayers) {
                    toSend.lobbies.push({name: game.name, numPlayers: game.players.length, maxPlayers: GameManager.maxPlayers, creator: game.players[0], hasPassword: Boolean(game.password)});
                }
                else if(game.phase != "lobby" && game.players.includes(req.query.username)){
                    toSend.rejoin.push({name: game.name, numPlayers: game.players.length, maxPlayers: GameManager.maxPlayers, creator: game.players[0]});
                }
            })
            res.send(toSend);
        }
    });
});

async function join(req, res){
    const roomName = (req.body.roomName) ? req.body.roomName : req.params.roomName;
    if(!roomName){
        return res.status(400).redirect('/');
    }
    await mongoGame.findOne({name:roomName}, async function(err, game) {
        if(err){
            console.log(err);
            return res.status(500).send("An unknown error occurred, please try again.");
        }
        
        if(!game){
            return res.status(404).send("No game with that name exists");
        }
        else if(game.phase != "lobby" && !game.players.includes(req.loggedIn.username)) {
            return res.status(400).send("That game has already started");
        }
        else if(game.players.length >= GameManager.maxPlayers){
            return res.status(400).send("That game is full");
        }

        // check if the game requires a password and is still in the lobby (no need to ask for password if the user is rejoining)
        if(game.password && game.phase == "lobby"){
            if(!req.body.password){
                return res.status(403).send("This game requires a password to join.")
            }
            else {
                const result = await bcrypt.compare(req.body.password, game.password)
                if (!result) {
                    return res.status(403).send("Incorrect password.");
                }
            }
        }

        const user = req.loggedIn.username;
        if(game.players.includes(user) && game.phase == "lobby") {
            return res.status(400).send("You're already in this game");
        }
        else {
            mongoUser.findOne({name:user}, function(err, player) {
                if(err){
                    console.log(err);
                    return res.status(500).send("An unknown error occurred, please try again.");
                }
                if(!player && !req.loggedIn.isGuest){
                    return res.status(401).send("Your username is invalid. Either you're trying to spoof or something has gone terribly wrong.");
                }
                else {
                    // we do this check in case the user is attempting to rejoin a game they left
                    if(!game.players.includes(user)){
                        game.players.push(user);
                    }
                    game.save();
                    // not currently used
                    // player.game = game._id;
                    // player.save();
                    if(req.method === "PUT"){
                        // joining from the game list
                        return res.send("Sucessfully joined")
                    }
                    else {
                        // joining from a lobby url
                        return res.redirect(`/lobby/${roomName}`);
                    }
                }
            });
        }
    });
}

router.put("/join", authenticate(), join);
router.get("/join/:roomName", authenticate(), join);

router.get('/lobby/:gameName', authenticate(), (req, res) => {
    // redirect to the page without the /game/
    res.redirect(`/lobby/${req.params.gameName}`);
});

// router.put("/start", (req, res) => {
//   mongoGame.findOne({name:req.body.roomName}, function(err, game) {
//     if(err) throw err;

//     if(!game){
//       res.status(404).send("No game with that name exists");
//     }
//     else if(game.phase != "lobby") {
//       res.status(400).send("That game has already started");
//     }
//     else {
//       if(game.players[0] != req.loggedIn.username){
//         res.status(401).send("Only the host can start the game");
//       }
//       else {
//         game.phase == "playing";

//       }
//     }
//   });
// });

async function generatePasswordHash(password){
    // generate a salt value
    let salt = await bcrypt.genSalt(10);
    // hash the plaintext password with the salt
    return bcrypt.hash(password, salt);
}

module.exports = router;