const express = require("express");
const router = express.Router();
const mongoUser = require("../mongodb_models/user_model.js");
const Authentication = require( "./authentication.js");
const authenticate = Authentication.authenticate;

const GameManager = require("../classes/gameManager.js");

const answers = {
    base: require('../cards/answers/a-base.js'),
    green: require('../cards/answers/a-green.js'),
    blue: require('../cards/answers/a-blue.js'),
    red: require('../cards/answers/a-red.js'),
    election2016: require('../cards/answers/a-2016.js'),
    fantasy: require('../cards/answers/a-fantasy.js'),
    food: require('../cards/answers/a-food.js'),
    holiday: require('../cards/answers/a-holiday.js'),
}

const questions = {
    base: require('../cards/questions/q-base.js'),
    green: require('../cards/questions/q-green.js'),
    blue: require('../cards/questions/q-blue.js'),
    red: require('../cards/questions/q-red.js'),
    election2016: require('../cards/questions/q-2016.js'),
    fantasy: require('../cards/questions/q-fantasy.js'),
    food: require('../cards/questions/q-food.js'),
    holiday: require('../cards/questions/q-holiday.js'),
}

router.get("/my-customs", authenticate(redirectOnFailure=true,allowGuests=false), (req, res) => {
    mongoUser.findOne({ name: req.loggedIn.username }, function(err, user){
        res.render('deck', {
            title: "My custom deck",
            username: req.loggedIn.username,
            questions: user.question_deck,
            answers: user.answer_deck,
        });
    })
});

//404 page for this router
// this needs to be above the deck catcher so it can be authoritative if the user is sent
// directly to it
router.get("/404", authenticate(redirectOnFailure=false), (req, res) => {
    return res.status(404).render('deck-404', {
        title: "404: deck not found",
        username: req.loggedIn.username,
        isGuest: req.loggedIn.isGuest,
        decks: GameManager.standardDecks,
    });
});

router.get("/:deck", authenticate(redirectOnFailure=false), (req, res) => {
    try {
        const deck = req.params.deck.toLowerCase();
        if(GameManager.standardDecks[deck]){
            return res.render('deck', {
                title: `${GameManager.standardDecks[deck].name} deck`,
                username: req.loggedIn.username,
                isGuest: req.loggedIn.isGuest,
                questions: questions[deck],
                answers: answers[deck],
            });
        }
    }
    catch {} //pass

    return res.redirect('/deck/404');

});

// anything not caught above goes to the 404 page
router.get("/*", authenticate(redirectOnFailure=false), (req, res) => {
    return res.redirect('/deck/404');
});

module.exports = router;