const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const mongoose = require('mongoose');
const mongoGame = require("./server/mongodb_models/game_model.js");
const mongoUser = require("./server/mongodb_models/user_model.js");
const Authentication = require( "./server/routes/authentication.js");
const authenticate = Authentication.authenticate;
const cookieParser = require('cookie-parser');
const cookie = require("cookie");
const jwt = require('jsonwebtoken');
const handlebars = require('express-handlebars');
const eventEmitter = require("./server/events.js");

const GameManager = require('./server/classes/gameManager.js');
var games = {};

// if our first cla is "local", then we use local ip, otherwise we use remote server
const hostname = '127.0.0.1';
const port = 8080;

const config = require("./server/config.js");
app.set('secret', config.secret);

app.engine('html', handlebars.engine({
  layoutsDir: './protected/layouts',
  extname: '.html',
  defaultLayout : 'menus',
  partialsDir: './protected/partials/',
  helpers: {
    titleCSS(str){return str.replaceAll(' ', '_').toLowerCase()},
    isdefined(value){return value !== undefined}
  }
}));
app.set('view engine', 'html');
app.set('views', './protected');


// connect to the mongo database
mongoose
  .connect(config.mongoURI,{ useNewUrlParser: true })
  .then(() => {
    console.log("DB connected");

    // remove old games on startup. These should only exist in the event of a crash or other error, and will cause further errors if kept
    mongoGame.deleteMany({}).then(() => {console.log("deleted old games")});
  })
  .catch(err => console.log(err));

app.use(cookieParser());

// use body parser so we can get info from POST and/or URL parameters
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(function (req, res, next) {
  res.messages = [];
  next();
});

// these handlebars variables are applied to every route
app.use(function (req, res, next) {
  res.locals = {
    isLightMode:(req.cookies.isLightMode == "true"),
  };
  next();
});

app.get('/', (req, res) => {
  if(req.loggedIn) {
    res.redirect('/menu');
  }
  else {
    res.redirect('/login');
  }
});

app.get('/login', (req, res) => {
  if(req.cookies.token){
    jwt.verify(req.cookies.token, config.secret, function(err) {
      if(err) {
        res.clearCookie('token').status(401).render('login', {title:"Login"});
      }
      else {
        res.redirect('/menu');
      }
    });
  }
  else{
    res.render('login', {title:"Login"});
  }
});

app.get('/register', (req, res) => {
  return res.redirect('/login');
});

app.get('/about', authenticate(redirectOnFailure=false), (req, res) => {
  return res.render('about', {title:"About", username: req.loggedIn.username, isGuest: req.loggedIn.isGuest});
});

app.get('/tutorial', authenticate(redirectOnFailure=false), (req, res) => {
  return res.render('tutorial', {title:"How to play", username: req.loggedIn.username, isGuest: req.loggedIn.isGuest});
});

app.get('/help', (req, res) => {
  return res.redirect('/tutorial');
});

app.get('/privacy', authenticate(redirectOnFailure=false), (req, res) => {
  return res.render('privacy', {title:"Privacy policy", username: req.loggedIn.username, isGuest: req.loggedIn.isGuest});
});

// app.get('/paqaqiwta', authenticate(redirectOnFailure=false), (req, res) => {
//   return res.render('paqaqiwta', {title:"PAQAQIWTA", username: req.loggedIn.username});
// });

app.get("/deck_editor", authenticate(redirectOnFailure=true,allowGuests=false), (req, res) => {
  return res.render("deck editor", {title:"Deck editor", username: req.loggedIn.username, isGuest: req.loggedIn.isGuest, chatCharacterLimit: GameManager.chatCharacterLimit});
});

app.get('/preferences', authenticate(redirectOnFailure=true,allowGuests=false), (req, res) => {
  mongoUser.findOne({ name: req.loggedIn.username }, function(err, user){
    if(err){
      return res.status(500).redirect('/menu');
    }
    if(user){
      res.render('preferences', {
        title:"Preferences",
        username: user.name,
        email: user.email,
        preferredColor: user.preferredColor,
        chatCharacterLimit: GameManager.chatCharacterLimit
      });
    }
    else {
      return res.status(401).redirect('/login');
    }
  });
});

app.get('/settings', (req, res) => {
  return res.redirect('/preferences');
});

app.get('/play/:gameName', authenticate(), (req, res) => {
  mongoUser.findOne({name:req.loggedIn.username}, function(err, user){
    if(user || req.loggedIn.isGuest){
      mongoGame.findOne({name:req.params.gameName}, function(err, gamedb){
        if(gamedb){
          if(gamedb.players.includes(req.loggedIn.username)){
            return res.render('play', {
              layout: false,
              title: req.params.gameName,
              username: req.loggedIn.username,
              isGuest: req.loggedIn.isGuest,
              volume: (user) ? user.volume : 1,
              gameName: req.params.gameName,
              chatCharacterLimit: GameManager.chatCharacterLimit
            });
          }
          else{
            let gameID = gamedb._id + '';
            if(games[gameID].playerIsGone(req.loggedIn.username)){
              return res.render('play', {
                layout: false,
                title: req.params.gameName,
                username: req.loggedIn.username,
                isGuest: req.loggedIn.isGuest,
                volume: (user) ? user.volume : 1,
                gameName: req.params.gameName,
                chatCharacterLimit: GameManager.chatCharacterLimit
              });
            }
            else {
              return res.status(403).redirect('/menu');
            }
          }
        }
        else {
          res.messages.push("A game with that name doesn't exist.");
          return res.status(404).redirect('/menu');
        }
      });
    }
    else {
      res.messages.push("You must be logged in to play.");
      return res.status(401).redirect('/');
    }
  });
});

app.get('/menu', authenticate(), (req, res) => {
  res.render('menu', {title:"Menu", username: req.loggedIn.username, isGuest: req.loggedIn.isGuest, messages:res.messages});
});

app.get('/lobby/:gameName', authenticate(), (req, res) => {
  mongoGame.findOne({name: req.params.gameName}, function(err, gamedb){
    if(err){
      res.messages.push("An unknown error occurred.");
      return res.status(500).redirect('/menu');
    }
    if(!gamedb){
      res.messages.push("A game with that name doesn't exist.");
      return res.status(404).redirect('/menu');
    }
    else if (gamedb.players.includes(req.loggedIn.username)){
      mongoUser.findOne({ name: req.loggedIn.username }, function(err, userdb){
        if(!userdb && !req.loggedIn.isGuest){
          // this shouldn't be possible, but just in case
          res.messages.push("You must be logged in to play.");
          return res.status(401).redirect('/login');
        }
        else if (gamedb.phase != "lobby"){
          res.redirect(`/play/${req.params.gameName}`);
        }
        else{
          res.render('lobby', {
            title: `${req.params.gameName} - Lobby`,
            gameName: req.params.gameName,
            username: req.loggedIn.username,
            isGuest: req.loggedIn.isGuest,
            preferredColor: (userdb) ? userdb.preferredColor : "",
            minHandSize: GameManager.minHandSize,
            maxHandSize: GameManager.maxHandSize,
            baseHandSize: GameManager.baseHandSize,
            minPoints: GameManager.minPoints,
            maxPoints: GameManager.maxPoints,
            basePoints: GameManager.basePoints,
            pointsForWinToCount: GameManager.pointsForWinToCount,
            standardDecks: GameManager.standardDecks,
            customCardCount: GameManager.customCardCount,
          });
        }
      });
    }
    else {
      return res.redirect(`/game/join/${req.params.gameName}`);
    }
  });
});

const usersRouter = require("./server/routes/user.js");
app.use("/user", usersRouter);

const gamesRouter = require("./server/routes/game.js");
app.use("/game", gamesRouter);

const decksRouter = require("./server/routes/deck.js");
app.use("/deck", decksRouter);

app.use(express.static("public"));

io.on('connection', (socket) => {
  const cookies = cookie.parse(socket.request.headers.cookie);
  const username = jwt.verify(cookies.token, app.get('secret')).username;
  if(!username){
    socket.disconnect();
  }

  console.log(`${username} connected`);

  socket.on('join', (gameName) => {

    jwt.verify(cookies.token, app.get('secret'), function(err, loggedIn) {
      if(err) {
        console.log(err);
        socket.disconnect();
      }
      else {
        mongoGame.findOne({name:gameName}, function(err, gamedb) {
          // Figure out what an error here could mean and handle it

          // if we couldn't find a game, don't do anything. This can sometimes happen if the server restarts after a crash
          if(!gamedb){
            socket.disconnect();
            return;
          }

          let gameID = gamedb._id + '';

          // if a game manager has not yet been created for this game, do so now
          if(!games[gameID]) {
            games[gameID] = new GameManager(gameID, gamedb.name, io);
          }

          let game = games[gameID];

          socket.join(gameID);
          socket.username = username;
          socket.gameID = gameID;
          socket.gameName = gameName;

          // if the game is in transition, we don't want to do anything
          if(game.phase == "lobby"){
            //cast the id to a string

            // add this player to the game manager
            game.addPlayer(username, socket)

          }
          else if(game.phase == "transition"){
            game.playerReconnect(username, socket);
          }
          else if(game.playerIsGone(username)){
            game.rejoinPlayer(username, socket);
          }
        });
      }
    });

  });

  socket.on('disconnect', (reason) => {
    console.log(`${username} disconnected because of ${reason}`);
    jwt.verify(cookies.token, app.get('secret'), function(err) {
      if(err) {
        // pass
        console.log(`Error verifying token of socket claiming to be user ${username}`);
      }
      else {
        mongoGame.findOne({name:socket.gameName}, function(err, gamedb) {
          // Figure out what an error here could mean and handle it
          if(err) {
            console.log(err);
            return;
          }

          // if we couldn't find a game, don't do anything. This can sometimes happen if the server restarts after a crash
          if(!gamedb){
            return;
          }

          //cast the id to a string
          let gameID = socket.gameID;

          if(games[gameID]) {

            let game = games[gameID];

            game.updatePlayerSocket(username, undefined);
            if(game.phase != "transition") {

              // remove the player from the game manager
              games[gameID].removePlayer(username)

              if(game.phase == "lobby"){
                // remove the user from the game's player list
                let index = gamedb.players.indexOf(username);
                gamedb.players.splice(index,1);
                gamedb.save();
              }

            }
          }
        });
      }
    });
  });

  socket.on('player start', (gameName) => {
    jwt.verify(cookies.token, app.get('secret'), function(err) {
      if(err) {
        // pass
        console.log(`Error verifying token of socket of user: ${username}`);
      }
      else {
        mongoGame.findOne({name:gameName}, function(err, gamedb) {

        const gameID = socket.gameID;

        if(!gamedb || !(gameID in games)){
          socket.disconnect();
          return;
        }
        if(games[gameID].isHost(username)){

            // set the phase to transition so we can halt the disconnect events
            gamedb.phase = "transition";
            gamedb.save()

            games[gameID].setPhase("transition");
            games[gameID].start();

            io.to(gameID).emit('start game', gameName);
          }
        });
      }
    });
  })

  socket.on('player change color', (color) =>{
    jwt.verify(cookies.token, app.get('secret'), function(err) {
      if(err) {
        // pass
        console.log(`Error verifying token of socket claiming to be user ${username}`);
      }
      else {
        if(games[socket.gameID]){
          games[socket.gameID].changePlayerColor(username, color);
        }
        else{
          socket.disconnect();
          return;
        }
      }
    });
  });

  socket.on('player update settings', (settingsDict) => {
    jwt.verify(cookies.token, app.get('secret'), function(err) {
      if(err) {
        // pass
        console.log(`Error verifying token of socket of user: ${username}`);
      }
      else {
        const gameID = socket.gameID;
        if(gameID in games){
          if(games[gameID].isHost(username)){
            games[gameID].updateSettings(settingsDict);
          }
        }
      }
    });
  });

  socket.on('player selection', (indexes) => {
    jwt.verify(cookies.token, app.get('secret'), function(err) {
      if(err) {
        // pass
        console.log(`Error verifying token of socket claiming to be user ${username}`);
      }
      else {
        if(games[socket.gameID]){
          games[socket.gameID].receiveSelection(username, indexes);
        }
        else{
          socket.disconnect();
          return;
        }
      }
    });
  });

  socket.on('player skip question', (value) => {
    console.log(`Received player skip: ${username} chose ${value}`);
    jwt.verify(cookies.token, app.get('secret'), function(err) {
      if(err) {
        // pass
        console.log(`Error verifying token of socket claiming to be user ${username}`);
      }
      else {
        if(games[socket.gameID]){
          games[socket.gameID].receiveSkip(username, value);
        }
        else{
          socket.disconnect();
          return;
        }
      }
    });
  });

  socket.on('player discard', (index, value) => {
    jwt.verify(cookies.token, app.get('secret'), function(err) {
      if(err) {
        // pass
        console.log(`Error verifying token of socket claiming to be user ${username}`);
      }
      else {
        if(games[socket.gameID]){
          games[socket.gameID].receiveDiscard(username, index, value);
        }
        else{
          socket.disconnect();
          return;
        }
      }
    });
  });

  socket.on('chat message', (message) => {
    if(message.length > GameManager.chatCharacterLimit){
      return;
    }
    jwt.verify(cookies.token, app.get('secret'), function(err) {
      if(err) {
        // pass
        console.log(`Error verifying token of socket claiming to be user ${username}`);
      }
      else {
        if(games[socket.gameID]){
          games[socket.gameID].processMessage(username, message);
        }
        else{
          socket.disconnect();
          return;
        }
      }
    });
  });

  socket.on('update volume', (volume) => {
    try{
      volume = Number(volume);
    }
    catch {
      return;
    }
    if(volume >= 0 && volume <= 2){
      jwt.verify(cookies.token, app.get('secret'), function(err, loggedIn) {
        if(!loggedIn.isGuest){
          mongoUser.findOne({name: loggedIn.username}, function(err, userdb) {
            userdb.volume = volume;
            userdb.save();
          });
        }
      });
    }
  });

});

eventEmitter.on('delete room', (roomID) => {
  // remove the reference to the room so that the garbage collector will get rid of it
  games[roomID] = null;
});

http.listen(port, hostname, () => {
  console.log(`listening on ${hostname}:${port}`);
});